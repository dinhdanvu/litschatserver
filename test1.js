var HashMap = require('hashmap');
var _ = require('underscore');
//---------------------Chat Socket--------------------------------------------
var hash_name_of_chat_rooms = new HashMap();//room (key:room_id, room_object)
var hash_users = new HashMap();//user (key:user_id_socket_id,user_object)
//---------------------Dinh nghia chat user-----------------------------------
function Chat_User_Object(_user_id,_token,_display_name,_avatar,_socket_id){
    this.id = _socket_id;
    this.user_id = _user_id;
    this.token = _token;
    this.display_name = _display_name;
    this.avatar = _avatar;
    this.socket_id = _socket_id;
    this.rooms_name = [];
}
Chat_User_Object.prototype.GetRooms = function(){
    return this.rooms_name;
}
Chat_User_Object.prototype.JoinRoom = function(room){
    var result = _.find(this.rooms_name, function (o) { return o == room; });
    if (!result)
    {
        this.rooms_name.push(room);
    }
}
Chat_User_Object.prototype.RemoveRoom = function(room){
    this.rooms_name = _.without(this.rooms_name, room);
}
Chat_User_Object.prototype.Print = function(){
    console.log(this.id + ':'+this.rooms_name.length + ' room ---> '+ this.rooms_name);
}
//-----------------------------------------------------------
function Room_Object(_room_name){
    this.room_name = _room_name;
    this.users_id = [];
}
Room_Object.prototype.GetUsers = function(){
    return this.users_id;
}
Room_Object.prototype.JoinUser = function(user){
    var result = _.find(this.users_id, function (o) { return o == user; });
    if (!result)
    {
        this.users_id.push(user);
    }
}
Room_Object.prototype.RemoveUser = function(user){
    this.users_id = _.without(this.users_id, user);
}
Room_Object.prototype.Print = function(){
    console.log(this.room_name + ':'+ this.users_id.length + ' user ---> '+ this.users_id);
}
Room_Object.prototype.IsUserIn = function(user){
    var result = _.find(this.users_id, function (o) { return o == user; });
    if (!result)
    {
        return false;
    }
    return true;
}
Room_Object.prototype.IsEmpty = function(){
    return (this.users_id.length == 0);
}
//-----------------------------------------------------------

function user_join_chat_room(_user_id,_token,_display_name,_avatar,_socket_id, room_name){
    var _room = hash_name_of_chat_rooms.get(room_name);
    if (!_room)//tao room
    {
        var new_room = new Room_Object(room_name);
        new_room.JoinUser(_socket_id);//userid and socket_id
        hash_name_of_chat_rooms.set(room_name,new_room);
        //console.log('hash_name_of_chat_rooms -->add new key-->'+room_name);
        //console.log(hash_name_of_chat_rooms);
        var _user = hash_users.get(_socket_id);//userobject
        if (!_user)//tao user
        {
            var new_user = new Chat_User_Object(_user_id,_token,_display_name,_avatar,_socket_id);
            new_user.JoinRoom(room_name);
            console.log('hash_users -->add new key-->'+_socket_id);
            hash_users.set(_socket_id,new_user);
        }
        else
        {
            _user.JoinRoom(room_name);
        }
    }
    else
    {
        var result = _room.IsUserIn(_socket_id)
        if (!result)
        {
            _room.JoinUser(_socket_id);
            var new_user = new Chat_User_Object(_user_id,_token,_display_name,_avatar,_socket_id);
            new_user.JoinRoom(room_name);
            hash_users.set(_socket_id,new_user);
        }

    }
}
function user_leave_chat_room(id, room_name){
    var _room = hash_name_of_chat_rooms.get(room_name);
    if (_room)
    {
        _room.RemoveUser(id);

        var _user = hash_users.get(id);

        if (_user)
        {
            _user.RemoveRoom(room_name);
            if (_room.IsEmpty())
            {
                console.log(room_name + 'empty!!!');
                hash_name_of_chat_rooms.remove(room_name);
            }

        }

    }
}
CheckUserRoom = function(user_id, socket_id, room_id){
    var u = hash_users.get(user_id + socket_id);
    console.log(user_id + socket_id);
    if (u)
    {

        var rerult = _.find(u.GetRooms(), function (o) { return o == room_id; });
        if (rerult)
        {
            //console.log('already in');
            return true;
        }
        else {

            return false;
        }
    }
    else
    {
        console.log('not in');
        return false;
    }

}
function logall(){
    hash_name_of_chat_rooms.forEach(function(value, key) {
        var room_object = value;
        value.Print();
    });
    console.log('-----------------------------------------');
    hash_users.forEach(function(value, key) {
        value.Print();
    });
}
user_join_chat_room("_user_id1","_token1","_display_name1","_avatar1","_user_id1socket_id1", "room_name_A");
user_join_chat_room("_user_id1","_token1","_display_name1","_avatar1","_user_id1socket_id1", "room_name_B");
user_join_chat_room("_user_id2","_token2","_display_name2","_avatar2","_user_id2socket_id2", "room_name_A");
user_leave_chat_room('__user_id1socket_id1','room_name_A')
user_leave_chat_room('__user_id2socket_id2','room_name_A')
user_join_chat_room("_user_id1","_token1","_display_name1","_avatar1","_user_id1socket_id1", "room_name_A");
logall();

console.log(CheckUserRoom('_user_id1','socket_id1','room_name_A'));