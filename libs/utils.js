/**
 * Created with JetBrains WebStorm.
 * User: dinhd
 * Date: 5/12/17
 * Time: 8:29 PM
 * To change this template use File | Settings | File Templates.
 */
var libs = process.cwd() + '/libs/';
var db = require(libs + 'db/mongoose');
var asyncLoop = require('node-async-loop');
var LocationObject = require(libs + 'model/locationobject');
var TimeObject = require(libs + 'model/timeobject');
var BodyObject = require(libs + 'model/bodyobject');
var TextObject = require(libs + 'model/textobject');
var LocationObject = require(libs + 'model/locationobject');
var InteractionObject = require(libs + 'model/interactionobject');
var VideoObject = require(libs + 'model/videoobject');
var ImageObject = require(libs + 'model/imageobject');
var EmbedPackage = require(libs + 'model/embedpackage');
var PrivacyObject = require(libs + 'model/privacyobject');
var TimeObjectResponse = require(libs + 'model/timeobjectresponse');
var PrivacyObjectResponse = require(libs + 'model/privacyobjectresponse');
var UserProfileObject = require(libs + 'model/userprofileobject');
var BodyObjectResponse = require(libs + 'model/bodyobjectresponse');
var TextObjectResponse = require(libs + 'model/textobjectresponse');
var VideoObjectResponse = require(libs + 'model/videoobjectresponse');
var ImageObjectResponse = require(libs + 'model/imageobjectresponse');
var MileStoneObject = require(libs + 'model/milestoneobject');
var MileStoneObjectResponse = require(libs + 'model/milestoneobjectresponse');
var User = require(libs + 'model/user');
var AccessToken = require(libs + 'model/accessToken');

function getImageObjectByID(image_object_id, callback){
    var jsonobject = new ImageObjectResponse();
    jsonobject._id = image_object_id;
    ImageObject.findOne({_id:image_object_id},function(err, image_object){
        if (err)
            callback(err);
        else
        {
            callback(null, image_object);
        }
    });
}
function createLocationObject(lat, long, callback){
    var locationobject = new LocationObject();
    var coordinates = [];
    if (lat)
        coordinates.push(long);
    if (long)
        coordinates.push(lat);
    locationobject.coordinates = coordinates;
    locationobject.save(function (err, locationobject_) {
        if (err)
            callback(err);
        else
            callback(null,locationobject_);
    })
}
function createTimeObject(callback){
    var timeobject = new TimeObject();
    var now = Date.now();
    timeobject.lastmodified_date = now;
    timeobject.created_date = now;
    timeobject.save(function (err, timeobject) {
        if (err)
            callback(err);
        else
            callback(null,timeobject);
    })
}
function getVideoObjectByID(video_object_id, callback){
    var jsonobject = new VideoObjectResponse();
    jsonobject._id = video_object_id;
    VideoObject.findOne({_id:video_object_id},function(err, video_object){
        if (err)
            callback(err);
        else
        {
            callback(null, video_object);
        }
    });
}
function getmilisecondtime(time_object){
    var jsonobject = new TimeObjectResponse();
    jsonobject.created_date = time_object.created_date;
    jsonobject.lastmodified_date = time_object.lastmodified_date;
    return jsonobject;
}
function getLocationObjectByID(location_object_id, callback){
    if (!location_object_id)
        callback(null, {});
    else
        LocationObject.findOne({_id: location_object_id}, function(err, location_object){
        if (err)
            callback(err);
        else if (location_object){
            var json_location = {
                lat : location_object.coordinates[1],
                long : location_object.coordinates[0]
            }
            callback(null, json_location);
        }
        else {
            callback(null, null);
        }
    });
}
function getTimeObjectByID(time_object_id, callback){
    if (!time_object_id)
        callback(null, {});
    else
    TimeObject.findOne({_id: time_object_id}, function(err, time_object){
        if (err)
            callback(err);
        else if (time_object){
            var json_time = getmilisecondtime(time_object);
            callback(null, json_time);
        }
        else {
            callback(null, null);
        }
    });
}
function getBodyObjectByID(body_object_id, callback){
    var jsonobject = new BodyObjectResponse();
    jsonobject._id = body_object_id;
    //console.log('body_object_id here--->'+body_object_id);
    BodyObject.findOne({_id:body_object_id},function(err, body_object){
        if (err)
        {
            callback(err);
        }

        else if (body_object)
        {
            //console.log('body_object here--->'+body_object);
            getTimeObjectByID(body_object.time, function(err, time_object){
                if (err)
                    callback(err);
                else
                {
                    jsonobject.time = time_object;
                    getLocationObjectByID(body_object.location, function(err, location_object){
                        if (err)
                            callback(err);
                        else {
                            //console.log('------+++++>');
                            jsonobject.location = location_object;
                            getTextObjectByID(body_object.text, function(err, text_object){
                                if (err)
                                    callback(err);
                                else {
                                    jsonobject.text = text_object;
                                    if (body_object.videos.length > 0){
                                        asyncLoop(body_object.videos, function (item, next)
                                        {
                                            getVideoObjectByID(item,function (err, video){
                                                if (err)
                                                {
                                                    next(err);
                                                    return;
                                                }
                                                else{
                                                    //console.log('--uu-->'+image);
                                                    jsonobject.videos.push(video);
                                                    next();
                                                }
                                            });

                                        }, function (err)
                                        {
                                            if (err)
                                            {
                                                callback(err)
                                            }
                                            else if (body_object.images.length > 0){
                                                asyncLoop(body_object.images, function (item, next)
                                                {
                                                    getImageObjectByID(item,function (err, image){
                                                        if (err)
                                                        {
                                                            next(err);
                                                            return;
                                                        }
                                                        else{
                                                            //console.log('--uu-->'+image);
                                                            jsonobject.images.push(image);
                                                            next();
                                                        }
                                                    });

                                                }, function (err)
                                                {
                                                    if (err)
                                                    {
                                                        callback(err)
                                                    }
                                                    else
                                                        callback(null, jsonobject);
                                                });

                                            }
                                            else
                                                callback(null, jsonobject);

                                        });


                                    }
                                    else if (body_object.images.length > 0){

                                        asyncLoop(body_object.images, function (item, next)
                                        {
                                            getImageObjectByID(item,function (err, image){
                                                if (err)
                                                {
                                                    next(err);
                                                    return;
                                                }
                                                else{
                                                    //console.log('--uu-->'+image);
                                                    jsonobject.images.push(image);
                                                    next();
                                                }
                                            });

                                        }, function (err)
                                        {
                                            if (err)
                                            {
                                                callback(err)
                                            }
                                            else
                                                callback(null, jsonobject);
                                        });

                                    }
                                    else
                                        callback(null, jsonobject);
                                }
                            });
                        }
                    });

                }
            });
        }
        else
            callback(null, {});
    });
}
function getTextObjectByID(text_object_id, callback){
    var jsonobject = new TextObjectResponse();
    jsonobject._id = text_object_id;
    if (!text_object_id)
        callback(null, {});
    else
    TextObject.findOne({_id:text_object_id},function(err, text_object){
        if (err)
            callback(err);
        else if (text_object)
        {
            getTimeObjectByID(text_object.time, function(err, time_object){
                if (err)
                    callback(err);
                else
                {
                    jsonobject.time = time_object;
                    if (text_object.location){
                        getLocationObjectByID(text_object.location, function(err, location_object){
                            if (err)
                                callback(err);
                            else {
                                var json_location = {
                                    lat : location_object.lat,
                                    long : location_object.long
                                }
                                jsonobject.location = json_location;
                                jsonobject.index = text_object.index;
                                jsonobject.content = text_object.content;
                                callback(null, jsonobject);
                            }
                        });
                    }
                    else{

                        jsonobject.index = text_object.index;
                        jsonobject.content = text_object.content;
                        callback(null, jsonobject);
                    }
                }
            });
        }


    });
}
function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
//----------------------------------------------------------------------------------------------------------------------------------
module.exports = {
    getLocationObjectByID: function (location_object_id, callback){
        if (!location_object_id)
            callback(null, {});
        else
        LocationObject.findOne({_id: location_object_id}, function(err, location_object){
            if (err)
                callback(err);
            else if (location_object){
                //console.log(location_object);
                var json_location = {
                    lat : location_object.coordinates[1],
                    long : location_object.coordinates[0]
                }
                callback(null, json_location);
            }
            else {
                callback(null, null);
            }
        });
    },
    getInteractionObjectByID: function(interaction_object_id, callback){
        InteractionObject.findOne(interaction_object_id, function (err, interaction_object) {
            if (err)
                callback(err)
            else{
                var json_interaction = {
                    like_count: interaction_object.likes.length,
                    comment_count: interaction_object.comments.length
                }
                callback(null, json_interaction);
            }
        });
    },
    getTimeObjectByID: function(time_object_id, callback){
        if (!time_object_id)
            callback(null, {});
        else
        TimeObject.findOne({_id: time_object_id}, function(err, time_object){
            if (err)
                callback(err);
            else if (time_object){
                var json_time = getmilisecondtime(time_object);
                callback(null, json_time);
            }
            else {
                callback(null, null);
            }
        });
    },
    getBodyObjectByID: function (body_object_id, callback){
        var jsonobject = new BodyObjectResponse();
        jsonobject._id = body_object_id;
        //console.log('body_object_id here--->'+body_object_id);
        BodyObject.findOne({_id:body_object_id},function(err, body_object){
            if (err)
            {
                callback(err);
            }

            else if (body_object)
            {
                //console.log('body_object here--->'+body_object);
                getTimeObjectByID(body_object.time, function(err, time_object){
                    if (err)
                        callback(err);
                    else
                    {
                        jsonobject.time = time_object;
                        getLocationObjectByID(body_object.location, function(err, location_object){
                            if (err)
                                callback(err);
                            else {
                                //console.log('------+++++>');
                                jsonobject.location = location_object;
                                getTextObjectByID(body_object.text, function(err, text_object){
                                    if (err)
                                        callback(err);
                                    else {
                                        jsonobject.text = text_object;
                                        if (body_object.videos.length > 0){
                                            asyncLoop(body_object.videos, function (item, next)
                                            {
                                                getVideoObjectByID(item,function (err, video){
                                                    if (err)
                                                    {
                                                        next(err);
                                                        return;
                                                    }
                                                    else{
                                                        //console.log('--uu-->'+image);
                                                        jsonobject.videos.push(video);
                                                        next();
                                                    }
                                                });

                                            }, function (err)
                                            {
                                                if (err)
                                                {
                                                    callback(err)
                                                }
                                                else if (body_object.images.length > 0){
                                                    asyncLoop(body_object.images, function (item, next)
                                                    {
                                                        getImageObjectByID(item,function (err, image){
                                                            if (err)
                                                            {
                                                                next(err);
                                                                return;
                                                            }
                                                            else{
                                                                //console.log('--uu-->'+image);
                                                                jsonobject.images.push(image);
                                                                next();
                                                            }
                                                        });

                                                    }, function (err)
                                                    {
                                                        if (err)
                                                        {
                                                            callback(err)
                                                        }
                                                        else
                                                            callback(null, jsonobject);
                                                    });

                                                }
                                                else
                                                    callback(null, jsonobject);

                                            });


                                        }
                                        else if (body_object.images.length > 0){

                                            asyncLoop(body_object.images, function (item, next)
                                            {
                                                getImageObjectByID(item,function (err, image){
                                                    if (err)
                                                    {
                                                        next(err);
                                                        return;
                                                    }
                                                    else{
                                                        //console.log('--uu-->'+image);
                                                        jsonobject.images.push(image);
                                                        next();
                                                    }
                                                });

                                            }, function (err)
                                            {
                                                if (err)
                                                {
                                                    callback(err)
                                                }
                                                else
                                                    callback(null, jsonobject);
                                            });

                                        }
                                        else
                                            callback(null, jsonobject);
                                    }
                                });
                            }
                        });

                    }
                });
            }
            else
                callback(null, {});
        });
    },
    getEmbedPackageByID: function(embed_package_id, callback){
        //todo
        EmbedPackage.findOne({_id:embed_package_id},function(err, embed_package){
            if (err)
                callback(err);
            else
                callback(null,embed_package);
        });
    },
    getmilisecondtime: function (time_object){
    var jsonobject = new TimeObjectResponse();
    jsonobject.created_date = time_object.created_date;
    jsonobject.lastmodified_date = time_object.lastmodified_date;
    return jsonobject;
    },
    getPrivacyObjectByID: function (privacy_object_id, callback){
    var jsonObject = new PrivacyObjectResponse();
    jsonObject._id = privacy_object_id;
    PrivacyObject.findOne({_id: privacy_object_id},function(err,privacy_object){
        if (err)
            callback(err);
        else if (privacy_object)
        {
            jsonObject.stricts = privacy_object.stricts;
            jsonObject.blacklist = privacy_object.blacklist;
            jsonObject.whitelist = privacy_object.whitelist;
            getTimeObjectByID(privacy_object.time, function(err,time_object){
                if (err)
                    callback(err)
                else if (time_object)
                {
                    jsonObject.time = time_object
                    //console.log(jsonObject);
                    getLocationObjectByID(privacy_object.location,function(err,location_object){
                        if (err)
                        {
                            callback(err)
                        }
                        else if (location_object)
                        {
                            jsonObject.location = location_object;
                            callback(null, jsonObject)
                        }
                        else
                            callback(null, jsonObject)
                    });
                }
            });
        }
    });
},
    getMileStoneByID: function(id, user_id, callback){
        MileStoneObject.findOne({_id: id}, function(err, milestone_object){
            if (err)
                callback(err);
            else if (milestone_object){
                console.log(milestone_object);
                getBodyObjectByID(milestone_object.content, function(err, content){
                    if (err)
                        callback(err);
                    else
                        callback(null, content);
                });
            }
            else {
                callback(null, {});
            }
        });
},
    getUserProfileByID: function (user_id, callback)
    {
        User.findOne({_id:user_id}, function(err,user){
            if (err)
            {
                //console.log(err.message);
                callback(err);
            }

            else{

                UserProfileObject.findOne({_id:user.profile}, function(err, profile){
                    if (err)
                        callback(err);
                    else
                        callback(null, profile);
                });
            }
        });
    },
    createPrivacyObject: function (privacyobject,callback) {
    if (privacyobject.location_object)
    {
        createLocationObject(privacyobject.location_object.lat, privacyobject.location_object.long, function (err, locationobject) {
            if (err)
                callback(err);
            else {
                createTimeObject(function (err, timeobject) {
                    if (err)
                        callback(err);
                    else {
                        var privacy_object = new PrivacyObject();
                        privacy_object.stricts = privacyobject.stricts;
                        privacy_object.whitelist = privacyobject.whitelist;
                        privacy_object.blacklist = privacyobject.blacklist;
                        privacy_object.enable = privacyobject.enable;
                        privacy_object.time = timeobject._id
                        privacy_object.location = locationobject._id
                        privacy_object.save(function (err, privacy_object) {
                            if (err)
                                callback(err);
                            else {
                                callback(null, privacy_object);
                            }
                        });
                    }
                });
            }
        });
    }
    else
    {
        createTimeObject(function (err, timeobject) {
            if (err)
                callback(err);
            else {
                var privacy_object = new PrivacyObject();
                privacy_object.stricts = privacyobject.stricts;
                privacy_object.whitelist = privacyobject.whitelist;
                privacy_object.blacklist = privacyobject.blacklist;
                privacy_object.enable = privacyobject.enable;
                privacy_object.time = timeobject._id
                privacy_object.save(function (err, privacy_object) {
                    if (err)
                        callback(err);
                    else {
                        callback(null, privacy_object);
                    }
                });
            }
        });
    }
},
    createTimeObject: function(callback){
        var timeobject = new TimeObject();
        var now = Date.now();
        timeobject.lastmodified_date = now;
        timeobject.created_date = now;
        timeobject.save(function (err, timeobject) {
            if (err)
                callback(err);
            else
                callback(null,timeobject);
        })
    },createTimeObjectAt: function(time_object, callback){
        var timeobject = new TimeObject();
        timeobject.lastmodified_date = time_object.lastmodified_date;
        timeobject.created_date = time_object.created_date;
        timeobject.save(function (err, timeobject) {
            if (err)
                callback(err);
            else
                callback(null,timeobject);
        })
    },
    createLocationObject: function(lat, long, callback){
        var locationobject = new LocationObject();
        var coordinates = [];
        if (lat)
            coordinates.push(long);
        if (long)
            coordinates.push(lat);
        locationobject.coordinates = coordinates;
        locationobject.save(function (err, locationobject_) {
            if (err)
                callback(err);
            else
                callback(null,locationobject_);
        })
    },
    createInteractionObject: function (callback) {
        createTimeObject(function (err, timeobject) {
            if (err)
                callback(err);
            else {
                var interaction_object = new InteractionObject();
                interaction_object.time = timeobject._id;
                interaction_object.save(function (err, interaction_object) {
                    if (err)
                        callback(err);
                    else
                        callback(null,interaction_object);
                });

            }
        });
    },
    createEmbedPackage: function (callback) {
        var embed_package = new EmbedPackage();
        var now = Date.now();
        embed_package.lastmodified_date = now;
        embed_package.created_date = now;
        embed_package.save(function (err, embed_package) {
            if (err)
                callback(err);
            else
                callback(null,embed_package);
        });
    },
    createTextObject: function (location_object,content,index,callback) {
    if (location_object)
    {
        var locationobject_text = new LocationObject();
        if (location_object.lat)
            locationobject_text.lat = location_object.lat;
        if (location_object.long)
            locationobject_text.long = location_object.long;
        if (location_object.blacklist)
            locationobject_text.blacklist = location_object.blacklist;

        locationobject_text.save(function (err, locationobject_text) {
            if (err)
                callback(err);
            else {
                createTimeObject(function (err, timeobject_text) {
                    if (err)
                        callback(err);
                    else {
                        var textobject = new TextObject({
                            index: parseInt(index),
                            content: content
                        });
                        textobject.time = timeobject_text._id
                        textobject.location = locationobject_text._id
                        textobject.save(function (err, textobject) {
                            if (err)
                                callback(err);
                            else {
                                callback(null,textobject);
                            }

                        });
                    }
                });
            }
        });
    }
    else
    {
        var timeobject_text = new TimeObject();
        var now = Date.now();
        timeobject_text.lastmodified_date = now;
        timeobject_text.created_date = now;
        timeobject_text.save(function (err, timeobject_text) {
            if (err)
                callback(err);
            else {
                var textobject = new TextObject({
                    index: parseInt(index),
                    content: content
                });
                textobject.time = timeobject_text._id
                textobject.save(function (err, textobject) {
                    if (err)
                        callback(err);
                    else {
                        callback(null,textobject);
                    }

                });
            }
        });
    }
    },
    createBodyObject: function (bodyobject,callback) {
    if (bodyobject.location_object)
    {
        createLocationObject(bodyobject.location_object.lat, bodyobject.location_object.long, function (err, locationobject) {
            if (err)
                callback(err);
            else {
                createTimeObject(function (err, timeobject) {
                    if (err)
                        callback(err);
                    else {
                        var body_object = new BodyObject();
                        body_object.text = bodyobject.text;
                        body_object.images = bodyobject.images;
                        body_object.videos = bodyobject.videos;
                        body_object.time = timeobject._id
                        body_object.location = locationobject._id
                        body_object.save(function (err, body_object) {
                            if (err)
                                callback(err);
                            else {
                                callback(null, body_object);
                            }
                        });
                    }
                });
            }
        });
    }
    else
    {
        createTimeObject(function (err, timeobject) {
            if (err)
                callback(err);
            else {
                var body_object = new BodyObject();
                body_object.text = bodyobject.text;
                body_object.images = bodyobject.images;
                body_object.videos = bodyobject.videos;
                body_object.time = timeobject._id
                body_object.save(function (err, body_object) {
                    if (err)
                        callback(err);
                    else {
                        callback(null, body_object);
                    }
                });
            }
        });
    }
},
    isValidate: function(user_object, callback)
{
    //console.log(user_object);
    if (IsJsonString(user_object))
    {
        var obj = JSON.parse(user_object);
        User.findOne({ username: obj.username }, function(err, user){
            if (err)
                callback(false);
            else
            {
                if (!user)
                    callback(false);
                else
                    AccessToken.findOne({ userId: user._id, token: obj.token }, function(err, token) {

                        if (err) {
                            callback(false);
                        }
                        else
                        {
                            if (token)
                            {
                                //console.log(colors.yellow(obj.username + '--> utils ----> validated'));
                                callback(true, token.userId);
                            }

                            else
                                callback(false);
                        }

                    });
            }

        });
    }


    return true;
},
    IsJsonString: function(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
},
    test: function(){
        console.log('utils');
    }
};