var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    MileStoneObjectSchema = mongoose.Schema({
        content: String,
        user_id: String
});

module.exports = mongoose.model('MileStoneObject', MileStoneObjectSchema);
