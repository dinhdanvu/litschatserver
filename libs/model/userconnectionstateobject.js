var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    UserConnectionStateObjectSchema = mongoose.Schema({
        sync_location: String,
        availability: String,
        time: String,
        location: String
});

module.exports = mongoose.model('UserConnectionStateObject', UserConnectionStateObjectSchema);
