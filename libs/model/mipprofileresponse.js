var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    MIPProfileResponseSchema = mongoose.Schema({
        title: String,
        address: String,
        address_location: {},
        content: {},
        url: {}
});

module.exports = mongoose.model('MIPProfileResponse', MIPProfileResponseSchema);
