var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

BodyObjectSchema = mongoose.Schema({
  text: String,
  images: [String],
  videos: [String],
  time: String,
  location: String
});

module.exports = mongoose.model('BodyObject', BodyObjectSchema);
