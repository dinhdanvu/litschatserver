var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    LikeObjectSchema = mongoose.Schema({
        user_profile: String,
        location: String,
        time: String
    });

module.exports = mongoose.model('LikeObject', LikeObjectSchema);
