var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

TextObjectSchema = mongoose.Schema({
  content: String,
  index: Number,
  time: String,
  location: String
});

module.exports = mongoose.model('TextObject', TextObjectSchema);
