var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    MIPProfileObjectSchema = mongoose.Schema({
        title: String,
        address: String,
        address_location: String,
        content: String,
        url: String
});

module.exports = mongoose.model('MIPProfileObject', MIPProfileObjectSchema);
