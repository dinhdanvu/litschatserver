var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

PrivacyObjectSchema = mongoose.Schema({
  enable: Boolean,
  whitelist:[String],
  blacklist:[String],
  stricts: [Number],
  time: String,
  location: String
});

module.exports = mongoose.model('PrivacyObject', PrivacyObjectSchema);
