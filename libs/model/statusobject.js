var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

StatusObjectSchema = mongoose.Schema({
  content: String,
  embed: String,
  categories: [String],
  time: String,
  location: String
});

module.exports = mongoose.model('StatusObject', StatusObjectSchema);
