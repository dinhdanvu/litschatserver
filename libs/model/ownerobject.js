var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    OwnerObjectSchema = mongoose.Schema({
        user_id: String,
        target_id: String,
        target_class: String,
        time : Number,
        location: String
    });

module.exports = mongoose.model('OwnerObject', OwnerObjectSchema);
