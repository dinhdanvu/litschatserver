var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

StatusPackageSchema = mongoose.Schema({
    privacy: String,
    interaction: String,
    user_profile: String,
    embed: String,
    content: String,
    MIP: [String],
    milestones: [String],
    time: String,
    location: String
});

module.exports = mongoose.model('StatusPackage', StatusPackageSchema);
