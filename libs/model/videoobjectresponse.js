var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

VideoObjectResponseSchema = mongoose.Schema({
	_id: String,
	content: String,
	index: Number,
    description: String,
	time: {},
	location: {}
});

module.exports = mongoose.model('VideoObjectResponse', VideoObjectResponseSchema);
