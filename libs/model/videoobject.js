var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

VideoObjectSchema = mongoose.Schema({
  content: String,
  index: Number,
  description: String,
  raw: String,
  time: String,
  location: String
});

module.exports = mongoose.model('VideoObject', VideoObjectSchema);
