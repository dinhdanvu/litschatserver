var mongoose = require('mongoose'),
    Schema = mongoose.Schema,

    PlaceObjectSchema = mongoose.Schema({
        referer_ids : [String],
        rank : Number,
        base_name : String
    });

module.exports = mongoose.model('PlaceObject', PlaceObjectSchema);
