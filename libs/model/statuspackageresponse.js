var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

StatusPackageResponseSchema = mongoose.Schema({
	_id: String,
	privacy: {},
    user_profile: {},
	content: {},
	time: {},
    like_count: Number,
    comment_count: Number,
	location: {}
});

module.exports = mongoose.model('StatusPackageResponse', StatusPackageResponseSchema);
