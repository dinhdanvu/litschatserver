var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

InteractionObjectSchema = mongoose.Schema({
    likes: [String],
    comments: [String],
    shares: [String],
    categories: [String],
    time: String,
    location: String
});

module.exports = mongoose.model('InteractionObject', InteractionObjectSchema);
