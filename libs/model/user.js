var mongoose = require('mongoose'),
	friends = require("mongoose-friends"),
	mongoosePaginate = require('mongoose-paginate'),
    crypto = require('crypto'),

    Schema = mongoose.Schema,

    User = new Schema({
        
        
        username: {
            type: String,
            unique: true,
            required: true
        },
		profile: String,
		connection: String,
		tracing_object: [String],
        emailconfirmed: {
            type: Boolean,
            default: false
        },
        emaillinkdate: Number,
        lastlogindate: Number,
		fcmtoken: {
            type: String,
            default: ""
        },
		statuses:[String],
		misstions:[String],
		journals:[String],
        hashedPassword: {
            type: String,
            required: true
        },
        salt: {
            type: String,
            required: true
        },
        created: Number
    });
User.plugin(friends());
User.plugin(mongoosePaginate);
User.methods.encryptPassword = function (password) {
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
    //more secure - return crypto.pbkdf2Sync(password, this.salt, 10000, 512).toString('hex');
};

User.virtual('userId')
    .get(function () {
        return this.id;
    });

User.virtual('password')
    .set(function (password) {
        this._plainPassword = password;
        this.salt = crypto.randomBytes(32).toString('hex');
        //more secure - this.salt = crypto.randomBytes(128).toString('hex');
        this.hashedPassword = this.encryptPassword(password);
    })
    .get(function () { return this._plainPassword; });


User.methods.checkPassword = function (password) {
    return this.encryptPassword(password) === this.hashedPassword;
};
User.methods.checkConfirm = function () {
    if ((this.emailconfirmed == false) && (this.phonenumberconfirmed == false))
        return false;
    return true;
};
module.exports = mongoose.model('User', User);
