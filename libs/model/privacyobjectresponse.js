var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

PrivacyObjectResponseSchema = mongoose.Schema({
	_id: String,
    stricts : [],
    blacklist : [],
    whitelist : [],
	time: {},
	location: {}
});

module.exports = mongoose.model('PrivacyObjectResponse', PrivacyObjectResponseSchema);
