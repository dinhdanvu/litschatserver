var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

UserProfileObjectSchema = mongoose.Schema({
  username: {
            type: String,
            unique: true,
            required: true
        },
  email: {
      type: String,
      unique: true,
      required: true
  },
  display_name: {
      type: String,
      default: ''
  },
  avatar: {
      type: String,
      default: ''
  },
  formatted_address: {
      type: String,
      default: ''
  },
  phonenumber: {
      type: String,
      default: ''
  },
  gender: Number,
  birthday: Number,
  category_packages: [String],
  location: String
});

module.exports = mongoose.model('UserProfileObject', UserProfileObjectSchema);
