var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

EmbedPackageSchema = mongoose.Schema({
    lastmodified_date : Number,
    created_date : Number,
    embed_objects: [String]
});

module.exports = mongoose.model('EmbedPackage', EmbedPackageSchema);
