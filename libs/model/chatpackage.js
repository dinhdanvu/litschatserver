var mongoose = require('mongoose'),
    Schema = mongoose.Schema,

    ChatPackageSchema = mongoose.Schema({
        title : String,
        chats : [String],
        users : [String],
        shared_content : [String],
        DCPs: [String],
        current_dcp: String
    });

module.exports = mongoose.model('ChatPackage', ChatPackageSchema);
