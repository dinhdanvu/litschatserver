var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    MissionObjectResponseSchema = mongoose.Schema({
        _id: String,
        title: String,
        description: String,
        end_time: Number,
        start_time: Number,
        content: {},
        embed: {},
        members: [],
        categories: [],
        routings: {},
        time: {},
        location: {}
});

module.exports = mongoose.model('MissionObjectResponse', MissionObjectResponseSchema);
