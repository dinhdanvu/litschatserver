var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    JournalPackageSchema = mongoose.Schema({
        title: String,
        root_content: String,
        root_name: String,
        missions: [String],
        statuses: [String],
        milestones: [String],
        embed: String,
        authors: [String],
        privacy: String,
        interaction: String,
        location: String,
        time: String
});

module.exports = mongoose.model('JournalPackage', JournalPackageSchema);
