var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

LocationObjectSchema = new Schema({
    name: String,
    coordinates: {
        type: [Number],  // [<longitude>, <latitude>]
        index: '2d'      // create the geospatial index
    }
});

module.exports = mongoose.model('LocationObject', LocationObjectSchema);
