var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

RoutingObjectResponseSchema = mongoose.Schema({
  time: {},
  location: {}
});

module.exports = mongoose.model('RoutingObjectResponse', RoutingObjectResponseSchema);
