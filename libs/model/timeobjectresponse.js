var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

TimeObjectResponseSchema = mongoose.Schema({
  lastmodified_date : Number,
  created_date : Number
});

module.exports = mongoose.model('TimeObjectResponse', TimeObjectResponseSchema);
