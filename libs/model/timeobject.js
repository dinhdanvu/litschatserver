var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

TimeObjectSchema = mongoose.Schema({
  lastmodified_date : Number,
  created_date : Number
});

module.exports = mongoose.model('TimeObject', TimeObjectSchema);
