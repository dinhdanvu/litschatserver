var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    SyncLocationObjectSchema = mongoose.Schema({
        location : String,
        time: String,
        privacy_object : String
    });

module.exports = mongoose.model('SyncLocationObject', SyncLocationObjectSchema);
