var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    CommentContentObjectSchema = mongoose.Schema({
    content: {},
    raw: {},
    time: String,
    location: String
});

module.exports = mongoose.model('CommentContentObject', CommentContentObjectSchema);
