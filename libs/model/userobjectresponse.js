var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

UserObjectResponseSchema = mongoose.Schema({
	_id: String,
    username: String,
	displayname: String,
	avatar: String
});

module.exports = mongoose.model('UserObjectResponse', UserObjectResponseSchema);
