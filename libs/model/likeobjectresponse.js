var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    LikeObjectResponseSchema = mongoose.Schema({
	_id: String,
    user_interaction_info: {},
	time: {},
	location: {}
});

module.exports = mongoose.model('LikeObjectResponse', LikeObjectResponseSchema);
