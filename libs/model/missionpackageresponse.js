var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

MissionPackageResponseSchema = mongoose.Schema({
	_id: String,
	privacy: {},
    user_profile: {},
	interaction: {},
    title: String,
    description:String,
    privacy:{},
    missions:[],
    members:{},
	time: {},
	location: {}
});

module.exports = mongoose.model('MissionPackageResponse', MissionPackageResponseSchema);
