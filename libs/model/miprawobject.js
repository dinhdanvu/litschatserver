var mongoose = require('mongoose'),
    Schema = mongoose.Schema,

    MipRawObjectSchema = mongoose.Schema({
        title: String,
        address: String,
        location: {},
        itsopen: {},
        category: {}
    });

module.exports = mongoose.model('MipRawObject', MipRawObjectSchema);
