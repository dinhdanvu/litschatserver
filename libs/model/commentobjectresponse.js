var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

CommentObjectResponseSchema = mongoose.Schema({
	_id: String,
    user_interaction_info: {},
	content: {},
	time: {},
	location: {}
});

module.exports = mongoose.model('CommentObjectResponse', CommentObjectResponseSchema);
