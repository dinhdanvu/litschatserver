var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

RoutingObjectSchema = mongoose.Schema({
  time: String,
  location: String
});

module.exports = mongoose.model('RoutingObject', RoutingObjectSchema);
