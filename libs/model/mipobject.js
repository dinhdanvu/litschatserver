var mongoose = require('mongoose'),
    Schema = mongoose.Schema,

    MIPObjectSchema = mongoose.Schema({
        owner: String,
        admins: [String],
        mip_admin_announcement: String,
        mip_user_feed: String,
        mip_profile: String,
        privacy: String,
        linked_mips: String,
        time: String,
        location: String
    });

module.exports = mongoose.model('MIPObject', MIPObjectSchema);
