var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

TextObjectResponseSchema = mongoose.Schema({
	_id: String,
	content: String,
	index: Number,
	time: {},
	location: {}
});

module.exports = mongoose.model('TextObjectResponse', TextObjectResponseSchema);
