var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    MissionObjectSchema = mongoose.Schema({
        title: String,
        description: String,
        end_time: String,
        start_time: String,
        content: String,
        embed: String,
        members: [String],
        categories: [String],
        routings: String,
        time: String,
        location: String
});

module.exports = mongoose.model('MissionObject', MissionObjectSchema);
