var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

BodyObjectResponseSchema = mongoose.Schema({
	_id: String,
	text: {},
	videos: [],
    images: [],
	time: {},
	location: {}
});

module.exports = mongoose.model('BodyObjectResponse', BodyObjectResponseSchema);
