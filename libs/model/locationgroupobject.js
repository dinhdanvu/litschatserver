var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

LocationGroupObjectSchema = mongoose.Schema({
    locations: [String]
});

module.exports = mongoose.model('LocationGroupObject', LocationGroupObjectSchema);
