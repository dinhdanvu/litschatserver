var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    MileStoneObjectResponseSchema = mongoose.Schema({
        content: {}
});

module.exports = mongoose.model('MileStoneObjectResponse', MileStoneObjectResponseSchema);
