var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

ImageObjectResponseSchema = mongoose.Schema({
	_id: String,
	content: String,
	index: Number,
    description: String,
	time: {},
	location: {}
});

module.exports = mongoose.model('ImageObjectResponse', ImageObjectResponseSchema);
