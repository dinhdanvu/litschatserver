var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    EmbedObjectSchema = mongoose.Schema({
    lastmodified_date : Number,
    created_date : Number,
    destination_object: String,
    destination_object_type : String,
    requirements : [String]
});

module.exports = mongoose.model('EmbedObject', EmbedObjectSchema);
