var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

StatusObjectResponseSchema = mongoose.Schema({
	_id: String,
	privacy: {},
	content: {},
	time: {},
	location: {}
});

module.exports = mongoose.model('StatusObjectResponse', StatusObjectResponseSchema);
