var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    CategoryObjectSchema = mongoose.Schema({
        area : Number,
        name : String,
        embed_object: String
});

module.exports = mongoose.model('CategoryObject)', CategoryObjectSchema);
