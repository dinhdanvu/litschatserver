var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    CategoryPackageSchema = mongoose.Schema({
        users_array: [String],
        categories: [String],
        time: String,
        location: String
});

module.exports = mongoose.model('CategoryPackage', CategoryPackageSchema);
