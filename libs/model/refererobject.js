var mongoose = require('mongoose'),
    Schema = mongoose.Schema,

    RefererObjectSchema = mongoose.Schema({
        routing : String,
        name : String,
        referer_caller : [String],
        referer_caller_object : String
    });

module.exports = mongoose.model('RefererObject', RefererObjectSchema);
