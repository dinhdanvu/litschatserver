var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    MipPageUserObjectSchema = mongoose.Schema({
        statuses: [String],
        missions: [String],
        journal: [String]
});

module.exports = mongoose.model('MipPageUserObject', MipPageUserObjectSchema);
