var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    MissionPackageSchema = mongoose.Schema({
        root_content: String,
        root_name : String,
        description: String,
        privacy: String,
        user_profile: String,
        title: String,
        interaction: String,
        missions: [String],
        tagged_users: [String],
        members: [String],
        milestones: [String],
        statuses: [String],
        location: String,
        time: String
});

module.exports = mongoose.model('MissionPackage', MissionPackageSchema);
