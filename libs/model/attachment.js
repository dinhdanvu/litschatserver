var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

AttachmentObjectSchema = mongoose.Schema({
    videos: {},
    pictures: {},
    time: String,
    location: String
});

module.exports = mongoose.model('AttachmentObject', AttachmentObjectSchema);
