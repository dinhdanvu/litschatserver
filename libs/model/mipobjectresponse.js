var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    MIPObjectResponseSchema = mongoose.Schema({
        owner: {},
        admins: [],
        mip_admin_announcement: {},
        mip_user_feed: {},
        mip_profile: {},
        privacy: {},
        linked_mips: {},
        time: {}
});

module.exports = mongoose.model('MIPObjectResponse', MIPObjectResponseSchema);
