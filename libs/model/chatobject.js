var mongoose = require('mongoose'),
    Schema = mongoose.Schema,

    ChatObjectSchema = mongoose.Schema({
        content : String,
        converstion : String,
        user_object : String,
        destination_DCP: String,
        index: Number
    });

module.exports = mongoose.model('ChatObject', ChatObjectSchema);
