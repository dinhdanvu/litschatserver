var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    MipPageAdminObjectSchema = mongoose.Schema({
        statuses: [String],
        missions: [String],
        journal: [String]
});

module.exports = mongoose.model('MipPageAdminObject', MipPageAdminObjectSchema);
