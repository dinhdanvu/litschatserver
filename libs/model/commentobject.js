var mongoose = require('mongoose'),
	Schema = mongoose.Schema,

    CommentObjectSchema = mongoose.Schema({
        user_profile: String,
        content: String,
        time: String,
        location: String
});

module.exports = mongoose.model('CommentObject', CommentObjectSchema);
