var mongoose = require('mongoose'),
    Schema = mongoose.Schema,

    ChatMessageSchema = mongoose.Schema({
        area : Number,
        username : String,
        lat: Number,
        long: Number,
        room : String,
        content: String,
        created: Number
    });

module.exports = mongoose.model('ChatMessage', ChatMessageSchema);
