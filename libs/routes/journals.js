//../api/users
var express = require('express');
var passport = require('passport');
var router = express.Router();
var libs = process.cwd() + '/libs/';
var config = require(libs + 'config');
var utils = require(libs + 'utils');
var host = config.get('host')
var port = config.get('port')

var libs = process.cwd() + '/libs/';
var utils = require(libs + 'utils');
var _ = require('underscore');
var db = require(libs + 'db/mongoose');
var asyncLoop = require('node-async-loop');
var User = require(libs + 'model/user');
var UserProfileObject = require(libs + 'model/userprofileobject');
var BodyObject = require(libs + 'model/bodyobject');
var LocationObject = require(libs + 'model/locationobject');
var TimeObject = require(libs + 'model/timeobject');
var JournalPackage = require(libs + 'model/journalpackage');
var RoutingObject = require(libs + 'model/routingobject');
var RoutingObjectResponse = require(libs + 'model/routingobjectresponse');
var PrivacyObject = require(libs + 'model/privacyobject');
var OwnerObject = require(libs + 'model/ownerobject');

var TimeObjectResponse = require(libs + 'model/timeobjectresponse');

var AccessToken = require(libs + 'model/accessToken');
router.use(passport.initialize());
function createOwnnerAuthor(author_ids, target_id, callback){
    var user_ids = [];
    asyncLoop(author_ids, function (item, next)
        {
            var owner_object = new OwnerObject ({
                user_id: item,
                target_id: target_id,
                target_class: "journal_author",
                time: Date.now()
            });
            //console.log(likes[i]);
            owner_object.save(function(err,owner_object){
                if(err)
                {
                    next(err);
                    return;
                }
                else
                {
                    user_ids.push(owner_object.user_id);
                    next();
                }

            });
        },

        function (err)
        {
            if (err)
            {
                callback(err)
            }
            else
                callback(null, user_ids);
        });
}
function checkOwnnerAuthor(author_ids, target_id, callback)
{
    //console.log('tag--->'+member_ids);
    OwnerObject.find({target_id:target_id, target_class:'journal_author', user_id : { $in : author_ids }},function(err, owner_objects){
        if (err)
            callback(err);
        else
        {
            for (var i = 0; i < owner_objects.length; i++)
                author_ids = _.without(author_ids, owner_objects[i].user_id);
            if (author_ids.length == 0)
                callback(null, []);
            else
            {
                createOwnnerAuthor(author_ids, target_id, function(err, list_ids){
                    if (err)
                        callback(err);
                    else {
                        callback(null, list_ids)
                    }
                });
            }
        }
    });
}

router.post('/', passport.authenticate('bearer', { session: false }), function(req,res){
    var check = true;
    if (!req.body.hasOwnProperty("journal"))
        check = false;
    else
    {
        if (!req.body.journal.hasOwnProperty("privacy_object"))
            check = false;
        else if (!req.body.journal.hasOwnProperty("title"))
            check = false;
        else if (!req.body.journal.hasOwnProperty("location_object"))
            check = false;
        else if (!req.body.journal.location_object.hasOwnProperty("lat"))
            check = false;
        else if (!req.body.journal.location_object.hasOwnProperty("long"))
            check = false;
//        else if (!req.body.journal.hasOwnProperty("missions"))
//            check = false;
//        else if (!req.body.journal.hasOwnProperty("statuses"))
//            check = false;
//        else if (!req.body.journal.hasOwnProperty("milestones"))
//            check = false;
        else if (!req.body.journal.hasOwnProperty("authors"))
            check = false;
        //check...
    }
    if (!check)
        res.json({
            error: 'error: bad data'
        })
    else
    {
        var journal_package = new JournalPackage();
        journal_package.title = req.body.journal.title;
        var privacyobject = new PrivacyObject();
        privacyobject.stricts = req.body.journal.privacy_object.stricts;
        if (req.body.journal.privacy_object.whitelist)
            privacyobject.whitelist = req.body.journal.privacy_object.whitelist;
        if (req.body.journal.privacy_object.blacklist)
            privacyobject.blacklist = req.body.journal.privacy_object.blacklist;
        utils.createPrivacyObject(privacyobject, function (err, privacy_object) {
            if (err) {
                res.json({
                    error: err.message
                });
            }
            else {
                journal_package.privacy = privacy_object._id;
                utils.createLocationObject(req.body.journal.location_object.lat, req.body.journal.location_object.long, function(err, location_object){
                    if (err)
                        res.json({
                            error: err.message
                        });
                    else {
                        journal_package.location = location_object._id;
                        utils.createTimeObject(function (err, timeobject) {
                            if (err)
                                callback(err);
                            else {
                                journal_package.time = timeobject._id;
                                utils.createInteractionObject(function (err, interaction_object) {
                                    if (err) {
                                        res.json({
                                            error: err.message
                                        });
                                    }
                                    else {
                                        journal_package.interaction = interaction_object._id;
                                        journal_package.save(function(err, journal_package){
                                            if (err) {
                                                res.json({
                                                    error: err.message
                                                });
                                            }
                                            else
                                            {
                                                var unique_authors = req.body.journal.authors.filter(function(elem, index, self) {
                                                    return index == self.indexOf(elem);
                                                })
                                                checkOwnnerAuthor(unique_authors, journal_package._id, function(err, authors){
                                                    if (err)
                                                        res.json({
                                                            error: err.message
                                                        });
                                                    else
                                                    {
                                                        JournalPackage.update({_id:journal_package._id},
                                                            { $addToSet: {authors: { $each:authors} }

                                                            },function (err, number) {
                                                                if (err)
                                                                {
                                                                    res.json({
                                                                        error: 'update journal package fail'
                                                                    });
                                                                }
                                                                else
                                                                {
                                                                    res.json({
                                                                        journal_package_id: journal_package._id
                                                                    });

                                                                }
                                                            }
                                                        )
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });



                    }
                });

            }
        });
    }
});
router.put('/insert_author', function(req, res){

});
module.exports = router;