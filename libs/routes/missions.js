//../api/users
var express = require('express');
var passport = require('passport');
var router = express.Router();
var libs = process.cwd() + '/libs/';
var config = require(libs + 'config');
var utils = require(libs + 'utils');
var host = config.get('host')
var port = config.get('port')

var libs = process.cwd() + '/libs/';
var utils = require(libs + 'utils');
var _ = require('underscore');
var db = require(libs + 'db/mongoose');
var asyncLoop = require('node-async-loop');
var User = require(libs + 'model/user');
var UserProfileObject = require(libs + 'model/userprofileobject');
var BodyObject = require(libs + 'model/bodyobject');
var LocationObject = require(libs + 'model/locationobject');
var MissionObject = require(libs + 'model/missionobject');
var MissionPackage = require(libs + 'model/missionpackage');
var PrivacyObject = require(libs + 'model/privacyobject');
var OwnerObject = require(libs + 'model/ownerobject');
var TimeObject = require(libs + 'model/timeobject');
var TimeObjectResponse = require(libs + 'model/timeobjectresponse');
var MissionPackageResponse = require(libs + 'model/missionpackageresponse');
var MissionObjectResponse = require(libs + 'model/missionobjectresponse');
var AccessToken = require(libs + 'model/accessToken');
router.use(passport.initialize());


function createMissionObject(mission, callback)
{
    var mission_object = new MissionObject();
    mission_object.title = mission.title;
    mission_object.description = mission.description;
    var start_time = parseFloat(mission.start_time);
    var timeobject = new TimeObject();
    timeobject.lastmodified_date = start_time;
    timeobject.created_date = start_time;
    utils.createTimeObjectAt(timeobject, function (err, timeobject) {
        if (err)
            callback(err);
        else {
            mission_object.start_time = timeobject._id;
            var end_time = parseFloat(mission.end_time);
            var timeobject = new TimeObject();
            timeobject.lastmodified_date = end_time;
            timeobject.created_date = end_time;
            utils.createTimeObjectAt(timeobject,function (err, timeobject) {
                if (err)
                    callback(err);
                else {
                    mission_object.end_time = timeobject._id;
                    utils.createTextObject(null, mission.body_object.text_object.content,mission.body_object.text_object.index, function(err, text_object){
                        if (err) {
                            callback(err);
                        }
                        else if(text_object){
                            var bodyobject = new BodyObject({
                                text: text_object._id
                            });
                            if (mission.body_object.videos.length > 0){
                                bodyobject.videos = mission.body_object.videos;
                            }
                            if (mission.body_object.images){
                                bodyobject.images = mission.body_object.images;
                            }

                            utils.createBodyObject(bodyobject, function (err, bodyobject) {
                                if (err) {
                                    callback(err);
                                }
                                else if (bodyobject){
                                    mission_object.content = bodyobject._id;
                                    mission_object.save(function(err, mission_object){
                                        if (err) {
                                            callback(err);
                                        }
                                        else if(mission_object){
                                            //console.log(mission_object);
                                            callback(null,mission_object);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });



}
function createAllMissions(missions, callback){
    var length = missions.length;
    //console.log(length);
    var mission_object_ids = [];
    if (length==0)
        callback(null,[]);
    else
        asyncLoop(missions, function (item, next)
        {
            createMissionObject(item,function(err,mission_object){
                if (err)
                {
                    next(err);
                    return;
                }
                else{
                    mission_object_ids.push(mission_object._id);
                    next();
                }
            });

        }, function (err)
        {
            if (err)
            {
                callback(err)
            }
            else
                callback(null, mission_object_ids);
        });

}
function createOwnnerTag(member_ids, package_id, callback){
    var user_ids = [];
    asyncLoop(member_ids, function (item, next)
    {
        var owner_object = new OwnerObject ({
            user_id: item,
            target_id: package_id,
            target_class: "tag_user_package",
            time: Date.now()
        });
        owner_object.save(function(err,owner_object){
            if (err)
            {
                next(err);
                return;
            }
            else{
                user_ids.push(owner_object.user_id);
                next();
            }
        });

    }, function (err)
    {
        if (err)
        {
            callback(err)
        }
        else
            callback(null, user_ids);
    });
}
function checkOwnnerTag(member_ids, package_id, callback)
{
    //console.log('tag--->'+member_ids);
    OwnerObject.find({target_id:package_id, target_class:'tag_user_package', user_id : { $in : member_ids }},function(err, owner_objects){
        if (err)
            callback(err);
        else
        {
            for (var i = 0; i < owner_objects.length; i++)
                member_ids = _.without(member_ids, owner_objects[i].user_id);
            if (member_ids.length == 0)
                callback(null, []);
            else
            {
                createOwnnerTag(member_ids, package_id, function(err, list_ids){
                    if (err)
                        callback(err);
                    else {
                        callback(null, list_ids)
                    }
                });
            }
        }
    });
}
router.post('/package/taguser', passport.authenticate('bearer', { session: false }), function(req,res){
    var check = true;
    if (!req.body.hasOwnProperty("members"))
    {
        check = false;
    }
    else if (!req.headers.mission_package_id)
    {
        check = false;
    }
    if (!check)
        res.json({
            error: 'bad data'
        });
    var unique_members = req.body.members.filter(function(elem, index, self) {
        return index == self.indexOf(elem);
    })
    MissionPackage.findOne( { _id : req.headers.mission_package_id},{ _id : 1, user_profile : 1 }, function(err, mission_package){
        if (err)
            res.json({
                error: err.message
            });
        else if (!mission_package)
            res.json({
                error: 'no mission package found'
            });
        else {
            if (mission_package.user_profile == req.user.userId)
            {
                checkOwnnerTag(unique_members, req.headers.mission_package_id, function(err, members){
                    if (members)
                    {
                        //console.log(members);
                        MissionPackage.update({_id:req.headers.mission_package_id},
                            { $addToSet: {members: { $each:members} }

                            },function (err, number) {
                                if (err)
                                {
                                    res.json({
                                        error: 'update mission package fail'
                                    });
                                }
                                else
                                {
                                    //console.log(number);

                                    res.json({
                                        message: 'OK'
                                    });
                                }
                            }
                        )
                    }
                });

            }

            else {
                res.json({
                    error: 'no permission'
                });
            }
        }
    });
});
router.post('/object/taguser', passport.authenticate('bearer', { session: false }), function(req,res){
    MissionPackage.findOne( { _id : req.headers.mission_package_id},{ _id : 1, user_profile : 1 }, function(err, mission_package){
        if (err)
            res.json({
                error: err.message
            });
        else if (!mission_package)
            res.json({
                error: 'no mission package found'
            });
        else {
            if (mission_package.user_profile == req.user.userId)
                MissionObject.findOne( {_id : req.headers.mission_object_id}, function(err, mission_object){
                if (err)
                    res.json({
                        error: err.message
                    });
                else if (!mission_object)
                    res.json({
                        error: 'no mission object found'
                    });
                else {
                    MissionObject.update({_id:mission_object._id},
                        { $addToSet: {members: { $each:req.body.members} }

                        },function (err, number) {
                            if (err)
                            {
                                res.json({
                                    error: 'update mission object fail'
                                });
                            }

                            else
                                res.json({
                                    message: 'OK'
                                });
                        }
                    )
                }
            });
            else {
                res.json({
                    error: 'no permission'
                });
            }
        }
    });
});
router.post('/', passport.authenticate('bearer', { session: false }), function(req,res){
    var check = true;
    if (!req.body.hasOwnProperty("missions"))
        check = false;
    else
    {
        if (req.body.missions.length==0)
            check = false;
        else if (!req.body.missions[0].hasOwnProperty("title"))
            check = false;
        else if (!req.body.missions[0].hasOwnProperty("description"))
            check = false;
        else if (!req.body.missions[0].hasOwnProperty("start_time"))
            check = false;
        else if (!req.body.missions[0].hasOwnProperty("end_time"))
            check = false;
        else if (!req.body.missions[0].hasOwnProperty("body_object"))
            check = false;
        else if (!req.body.missions[0].body_object.hasOwnProperty("text_object"))
            check = false;
        else if (!req.body.missions[0].body_object.text_object.hasOwnProperty("content"))
            check = false;
        else if (!req.body.missions[0].body_object.text_object.hasOwnProperty("index"))
            check = false;
    }
    //console.log(check);
    if (!check)
        res.json({
            error: 'error: bad data'
        })
    else
    {
        MissionPackage.findOne( { _id : req.headers._id},{ _id : 1 }, function(err, mission_package){
            if (err)
                res.json({
                    error: err.message
                });
            else if (!mission_package)
                res.json({
                    error: 'no mission package found'
                });
            else {
                createAllMissions(req.body.missions, function(err, mission_array_ids){
                    if (err)
                        res.json({
                            error: err.message
                        })
                    else if (mission_array_ids.length > 0)
                    {
                        var missions_ids = mission_array_ids;
                        MissionPackage.update({_id:mission_package._id},
                            {$pushAll: {
                                "missions":mission_array_ids
                            }
                            },function (err, number) {
                                if (err)
                                {
                                    res.json({
                                        error: 'update mission package fail'
                                    });
                                }

                                else
                                    res.json({
                                        message: 'OK'
                                    });
                            }
                        )


                    }
                    else if (mission_array_ids.lenth==0)
                    {
                        res.json({
                            error: 'save missions fail'
                        })
                    }
                });
            }
        });

    }
});
router.post('/package', passport.authenticate('bearer', { session: false }), function(req,res){
    var check = true;
    if (!req.body.hasOwnProperty("mission_package"))
        check = false;
    else
    {
        if (!req.body.mission_package.hasOwnProperty("title"))
            check = false;
        else if (!req.body.mission_package.hasOwnProperty("description"))
            check = false;
        else if (!req.body.mission_package.hasOwnProperty("privacy_object"))
            check = false;
        else if (!req.body.mission_package.hasOwnProperty("missions"))
            check = false;
        else if (!req.body.mission_package.hasOwnProperty("members"))
            check = false;
        else if (!req.body.mission_package.hasOwnProperty("location_object"))
            check = false;
        else if (!req.body.mission_package.location_object.hasOwnProperty("long"))
            check = false;
        else if (!req.body.mission_package.location_object.hasOwnProperty("lat"))
            check = false;
        else if (req.body.mission_package.missions.length==0)
            check = false;
        else if (!req.body.mission_package.missions[0].hasOwnProperty("title"))
            check = false;
        else if (!req.body.mission_package.missions[0].hasOwnProperty("description"))
            check = false;
        else if (!req.body.mission_package.missions[0].hasOwnProperty("start_time"))
            check = false;
        else if (!req.body.mission_package.missions[0].hasOwnProperty("end_time"))
            check = false;
        else if (!req.body.mission_package.missions[0].hasOwnProperty("body_object"))
            check = false;
        else if (!req.body.mission_package.missions[0].body_object.hasOwnProperty("text_object"))
            check = false;
        else if (!req.body.mission_package.missions[0].body_object.text_object.hasOwnProperty("content"))
            check = false;
        else if (!req.body.mission_package.missions[0].body_object.text_object.hasOwnProperty("index"))
            check = false;
    }
    //console.log(check);
    if (!check)
        res.json({
            error: 'error: bad data'
        })
    else
    {
        var mission_package = new MissionPackage();
        mission_package.members = req.body.mission_package.members;

        mission_package.description = req.body.mission_package.description;
        mission_package.title = req.body.mission_package.title;
        createAllMissions(req.body.mission_package.missions, function(err, mission_array_ids){
            if (err)
                res.json({
                    error: err.message
                })
            else if (mission_array_ids.length > 0)
            {
                //console.log(mission_array_ids);
                mission_package.missions = mission_array_ids;

                utils.createLocationObject(req.body.mission_package.location_object.lat, req.body.mission_package.location_object.long, function (err, locationobject) {
                    if (err) {
                        res.json({
                            error: err.message
                        })
                    }
                    else
                    {
                        if(!locationobject)
                        {
                            var err = {
                                message:'save locationobject fail'
                            }

                            callback(err);
                        }
                        else
                        {
                            mission_package.location = locationobject._id;
                            utils.createInteractionObject(function (err, interaction_object) {
                                if (err) {
                                    res.json({
                                        error: 'Create Interaction Object error'
                                    });
                                }
                                else {
                                    mission_package.interaction = interaction_object._id;
                                    utils.createEmbedPackage(function (err, embed_package) {
                                        if (err) {
                                            res.json({
                                                error: 'Create Embed Package error'
                                            });
                                        }
                                        else {
                                            mission_package.embed = embed_package._id;
                                            var privacyobject = new PrivacyObject();
                                            privacyobject.stricts = req.body.mission_package.privacy_object.stricts;
                                            if (req.body.mission_package.privacy_object.whitelist)
                                                privacyobject.whitelist = req.body.mission_package.privacy_object.whitelist;
                                            if (req.body.mission_package.privacy_object.blacklist)
                                                privacyobject.blacklist = req.body.mission_package.privacy_object.blacklist;
//                                            if (req.body.mission_package.privacy.enable != undefined)
//                                                privacyobject.enable = req.body.mission_package.privacy.enable;

                                            utils.createPrivacyObject(privacyobject, function (err, privacyobject) {
                                                if (err) {
                                                    res.json({
                                                        error: err.message
                                                    });
                                                }
                                                else {
                                                    mission_package.privacy = privacyobject._id;

                                                    utils.createTimeObject(function (err, timeobject) {
                                                        if (err)
                                                            callback(err);
                                                        else {
                                                            //console.log('dd'+req.user.userId);
                                                            mission_package.user_profile = req.user.userId;
                                                            mission_package.time = timeobject._id;
                                                            mission_package.save(function(err, mission_package){
                                                                if (err)
                                                                    res.json({
                                                                        error: err.message
                                                                    })
                                                                else{
                                                                    var ownerobject = new OwnerObject({
                                                                        user_id: req.user.userId,
                                                                        target_id: mission_package._id,
                                                                        target_class: 'post_mission_package',
                                                                        time: Date.now()
                                                                    });
                                                                    //console.log(ownerobject);
                                                                    ownerobject.save(function (err, ownerobject) {
                                                                        if (err)
                                                                            callback(err);
                                                                        else {
                                                                            checkOwnnerTag(req.body.mission_package.members, mission_package._id, function(err, members){
                                                                                if (members)
                                                                                {
                                                                                    res.json({
                                                                                        mission_package_id: mission_package._id
                                                                                    });
                                                                                }
                                                                            });

                                                                        }
                                                                    });
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            }
            else if (mission_array_ids.length==0)
            {
                res.json({
                    error: 'save missions fail'
                })
            }
        });
    }
});
function getMissionObjectByID(mission_object_id, callback){
    var jsonobject = new MissionObjectResponse();
    jsonobject._id = mission_object_id;

    MissionObject.findOne({_id:mission_object_id},function(err, mission_object){
        if (err)
            callback(err);
        else if (mission_object)
        {
            jsonobject.title = mission_object.title;
            jsonobject.description = mission_object.description;
            utils.getTimeObjectByID(mission_object.start_time, function(err, time_object){
                if (err)
                    callback(err);
                else if (time_object)
                {
                    jsonobject.start_time = utils.getmilisecondtime(time_object);
                    utils.getTimeObjectByID(mission_object.end_time, function(err, time_object){
                        if (err)
                            callback(err);
                        else if (time_object)
                        {
                            jsonobject.end_time = utils.getmilisecondtime(time_object);
                            utils.getBodyObjectByID(mission_object.content, function(err, body_object){
                                if (err)
                                    callback(err);
                                else {
                                    jsonobject.content = body_object;
                                    if (!mission_object.embed)
                                    {
                                        if (mission_object.members.length == 0)
                                        {
                                            if (!mission_object.routings)
                                            {
                                                callback(null, jsonobject);
                                            }
                                            else
                                            {

                                            }
                                        }
                                        else
                                        {

                                        }
                                    }
                                    else
                                    {
                                        //todo
                                        //getEmbedPackageByID(mission_object.)
                                    }

                                }
                            });

                        }
                    });
                }
            });
        }
    });
}
function getAllMissions(ids, callback){
    var statuspackages = [];
    asyncLoop(ids, function (item, next)
    {
        getMissionObjectByID(item,function (err, statuspackage){
            if (err)
            {
                next(err);
                return;
            }
            else{
                statuspackages.push(statuspackage);
                next();
            }
        });

    }, function (err)
    {
        if (err)
        {
            callback(err)
        }
        else
            callback(null, statuspackages);
    });
}
function getMissionPackageByID(mission_package_id, callback){
    var jsonObject = new MissionPackageResponse();
    jsonObject._id = mission_package_id;
    //console.log(mission_package_id);
    MissionPackage.findOne({_id:mission_package_id},function(err, mission_package){
        if (err)
            callback(err);
        else if (mission_package)
        {
            //console.log('-->'+mission_package);
            utils.getTimeObjectByID(mission_package.time,function(err,time_object){
                if (err)
                    callback(err)
                else if (time_object)
                {
                    jsonObject.time = time_object;
                    //console.log(jsonObject);
                    utils.getLocationObjectByID(mission_package.location, function(err,location_object){
                        if (err)
                        {
                            callback(err)
                        }
                        else if (location_object)
                        {
                            //console.log('location object here--->'+location_object);

                            jsonObject.location = location_object;
                            utils.getPrivacyObjectByID(mission_package.privacy, function(err, privacy_object){
                                if (err)
                                    callback(err)
                                else
                                {
                                    if (privacy_object)
                                    {
                                        var json_privacy = {
                                            stricts:privacy_object.stricts,
                                            enable: privacy_object.enable,
                                            whitelist:privacy_object.whitelist,
                                            blacklist:privacy_object.blacklist,
                                            time: privacy_object.time,
                                            location: privacy_object.location
                                        }
                                        jsonObject.privacy = json_privacy;
                                        if (mission_package.user_profile)
                                        {
                                            utils.getUserProfileByID(mission_package.user_profile, function(err,user_profile){
                                                if (err)
                                                    callback(err)
                                                else{
                                                    if (user_profile)
                                                    {
                                                        //console.log('-->'+user_profile);
                                                        var json = {
                                                            username:user_profile.username,
                                                            display_name: user_profile.display_name,
                                                            email: user_profile.email,
                                                            display_name: user_profile.email,
                                                            avatar: user_profile.avatar,
                                                            formatted_address: user_profile.formatted_address,
                                                            phonenumber: user_profile.phonenumber,
                                                            gender: user_profile.gender,
                                                            location: user_profile.location
                                                        }
                                                        jsonObject.user_profile = json;
                                                        getAllMissions(mission_package.missions, function (err, missions_list){
                                                            if (err)
                                                                callback(err)
                                                            else
                                                            {
                                                                jsonObject.missions = missions_list;
                                                                callback(null, jsonObject);
                                                            }

                                                        });

                                                    }
                                                    else
                                                        callback(null, jsonObject);
                                                }
                                            });
                                        }

                                        else {
                                            callback(null, jsonObject);
                                        }
                                    }
                                }
                            });
                            //console.log('-->'+jsonObject);

                        }

                    });
                }
            });
        }
    });
}
function getMissionPackageInfoByID(misstion_package_id, callback){
    //console.log(misstion_package_id);
    MissionPackage.findOne({  _id: misstion_package_id}, function (err, misstion_package) {
        if (err) {
            res.json({
                error: err.message
            })
        }
        else
        {
            var json_object;
            if (misstion_package){
                json_object = {
                    _id: misstion_package._id,
                    title: misstion_package.title
                };
            }
            else json_object = {};
            callback(null, json_object);
        }
    });
}
function getAllMissionPackageByListId(ids, callback){

    //console.log(owner_objects);
    var length = ids.length;
    //console.log(length);
    var mission_packages = [];
    if (length==0)
        callback(null,[]);
    else
        asyncLoop(ids, function (item, next)
        {
            getMissionPackageInfoByID(item._id,function(err,mission_package){
                if (err)
                {
                    next(err);
                    return;
                }
                else{
                    mission_packages.push(mission_package);
                    next();
                }
            });

        }, function (err)
        {
            if (err)
            {
                callback(err)
            }
            else
            {
                var mission_packages_result = [];
                for (j = 0; j < mission_packages.length; j++)
                {
                    if (mission_packages[j]._id)
                        mission_packages_result.push(mission_packages[j]);
                }
                callback(null, mission_packages_result);
            }
        });


}

function getMemberInfoByID(user_id, callback)
{
    var jsonobject = {
        username:"",
        display_name: ""
    };
    //console.log(user_id);
    User.findOne({_id:user_id},function(err, user_object){
        if (err)
            callback(err);
        else if (user_object)
        {
            //console.log(user_object.profile);
            UserProfileObject.findOne({_id:user_object.profile},function(err, user_profile_object){
                if (err)
                    callback(err);
                else if (user_profile_object)
                {
                    //console.log(user_profile_object);
                    jsonobject.username = user_profile_object.username;
                    jsonobject.display_name = user_profile_object.display_name;
                    var model = {
                        userId: user_id
                    };
                    //console.log(model);
                    AccessToken.findOne({userId: user_id}, function(err, access_token){
                        if (err)
                            callback(err);
                        else
                        {
                            if (!access_token)
                                jsonobject.token = '';
                            else
                                jsonobject.token = access_token.token;
                            callback(null, jsonobject);
                        }
                    });

                }
                else
                    callback(null, {});
            });
        }
    });
}
function getAllMissionPackageByOwnerObject(owner_objects, callback){

    //console.log(owner_objects);
    var length = owner_objects.length;
    //console.log(length);
    var mission_packages = [];
    if (length==0)
        callback(null,[]);
    else
        asyncLoop(owner_objects, function (item, next)
        {
            getMissionPackageInfoByID(item.target_id,function(err, mission_package){
                if (err)
                {
                    next(err);
                    return;
                }
                else{
                    mission_packages.push(mission_package);
                    next();
                }
            });

        }, function (err)
        {
            if (err)
            {
                callback(err)
            }
            else
            {
                var mission_packages_result = [];
                for (j = 0; j < mission_packages.length; j++)
                {
                    if (mission_packages[j]._id)
                        mission_packages_result.push(mission_packages[j]);
                }
                callback(null, mission_packages_result);
            }
        });
}
function getMissionMembers(members, callback){

    //console.log(members);
    var length = members.length;
    //console.log(length);
    var members_list = [];
    if (length==0)
        callback(null,[]);
    else
        asyncLoop(members, function (item, next)
        {
            getMemberInfoByID(item.user_id,function(err,members){
                if (err)
                {
                    next(err);
                    return;
                }
                else{
                    members_list.push(members);
                    next();
                }
            });

        }, function (err)
        {
            if (err)
            {
                callback(err)
            }
            else
            {
                var members_list_result = [];
                for (j = 0; j < members_list.length; j++)
                {
                    if (members_list[j].username)
                        members_list_result.push(members_list[j]);
                }
                callback(null, members_list_result);
            }
        });
}
router.get('/all_room', function(req, res){
    MissionPackage.find( {},{ _id : 1 }, function(err, status_packages){
        if (err)
            res.json({
                user_missions: []
            });
        else if (!status_packages)
            res.json({
                user_missions: []
            });
        else {
            getAllMissionPackageByListId(status_packages,function (err, user_missions){
                if (err)
                    res.json({
                        user_missions: []
                    });
                else if (user_missions){
                    res.json({
                        user_missions: user_missions
                    });
                }
                else {
                    res.json({
                        user_missions: []
                    });
                }
            });
        }
    });
});
router.get('/mission_members', function(req, res){

    MissionPackage.findOne( { _id : req.headers._id},{ _id:1, members : 1 }, function(err, members){
        if (err)
            res.json({
                mission_members: []
            });
        else if (!members)
            res.json({
                mission_members: []
            });
        else {
            if (members.members.length==0)
                res.json({
                    mission_members: []
                });
            else
            {
                //console.log(members);
                OwnerObject.find({target_class : { $in : ['tag_user_package','post_mission_package'] }, target_id : members._id},function(err, owner_objects){
                    if (err)
                        res.json({
                            mission_members: []
                        });
                    else
                    {
                        if (owner_objects.length == 0)
                            res.json({
                                mission_members: []
                            });
                        else
                        {
                            getMissionMembers(owner_objects,function (err, members){
                                if (err)
                                    res.json({
                                        mission_members: []
                                    });
                                else if (members){
                                    res.json({
                                        mission_members: members
                                    });
                                }
                                else {
                                    res.json({
                                        mission_members: []
                                    });
                                }
                            });
                        }
                    }
                });

            }

        }
    });
});
router.get('/user_missions', passport.authenticate('bearer', { session: false }), function(req, res){
    //console.log(req.user.userId);

    OwnerObject.find({target_class : { $in : ['tag_user_package','post_mission_package'] }, user_id : req.user.userId},function(err, owner_objects){
        if (err)
            callback(err.message);
        else
        {
            if (owner_objects.length == 0)
                res.json({
                    user_missions: []
                });
            else
            {
                getAllMissionPackageByOwnerObject(owner_objects, function(err, mission_packages){
                    if (err)
                        callback(err);
                    else
                    {
                        res.json({
                            user_missions: mission_packages
                        });
                    }
                });
            }
        }
    });
});
router.get('/id', function(req, res){
    MissionPackage.findOne( { _id : req.headers._id},{ _id : 1 }, function(err, status_package){
        if (err)
            res.json({
                error: err.message
            });
        else if (!status_package)
            res.json({
                error: 'no mission package found'
            });
        else {
            getMissionPackageByID(req.headers._id,function (err, mission_package){
                if (err)
                    res.json({
                        error: err.message
                    });
                else if (mission_package){
                    res.json({
                        mission_package: mission_package
                    });
                }
                else {
                    res.json({
                        error: 'no mission package found'
                    });
                }
            });
        }
    });
});
function getAllMissionPackage(ids, callback){
    var length_of_result = ids.length;
    var missionspackages = [];
    asyncLoop(ids, function (item, next)
    {
        getMissionPackageByID(item._id,function (err, missionpackage){
            if (err)
            {
                next(err);
                return;
            }
            else{
                missionspackages.push(missionpackage);
                next();
            }
        });

    }, function (err)
    {
        if (err)
        {
            callback(err)
        }
        else
        {
            callback(null, missionspackages);
        }
    });

}
router.get('/', function(req, res){
    LocationObject.find({ coordinates :
    { $geoWithin :{ $box : [ [ req.headers.left , req.headers.bottom ] , [ req.headers.right , req.headers.top ] ]} }
    },function(err, locationlbject) {
        if(err){
            //console.log('error');
            res.json({
                err: err.message
            });
        }
        else {
            if (locationlbject.length==0){
                //console.log('not found');
                res.json({
                    message: 'not found'
                });
            }
            else {

                var _ids = [];
                var length = locationlbject.length;

                for (var j = 0; j < length; j++)
                    _ids.push(locationlbject[j]._id);
                MissionPackage.find( { pinned_location : { $in : _ids } },{ _id : 1 }, function(err, result){
                    if (result)
                    {
                        console.log(result.length + ' mission(s) found');
                        if (result.length > 0)
                            getAllMissionPackage(result, function (err, missions_packages){
                                if (err)
                                    res.json({
                                        message : err.message
                                    });
                                else
                                    res.json({
                                        missions_packages : missions_packages
                                    });
                            });
                        else
                            res.json({
                                missions_packages : []
                            });
                    }
                });

            }
        }
    });
});
module.exports = router;