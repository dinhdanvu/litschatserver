//../api/users
var express = require('express');
var passport = require('passport');
var router = express.Router();
var libs = process.cwd() + '/libs/';
var config = require(libs + 'config');
var utils = require(libs + 'utils');
var host = config.get('host')
var port = config.get('port')

var libs = process.cwd() + '/libs/';
var utils = require(libs + 'utils');
var _ = require('underscore');
var db = require(libs + 'db/mongoose');
var asyncLoop = require('node-async-loop');
var User = require(libs + 'model/user');
var UserProfileObject = require(libs + 'model/userprofileobject');
var BodyObject = require(libs + 'model/bodyobject');
var LocationObject = require(libs + 'model/locationobject');
var MIPObject = require(libs + 'model/mipobject');
var MipPageAdminObject = require(libs + 'model/mippageadminobject');
var MipPageUserObject = require(libs + 'model/mippageuserobject');
var MIPProfileObject = require(libs + 'model/mipprofile');
var MipRawObject = require(libs + 'model/miprawobject');
var MIPObjectResponse = require(libs + 'model/mipobjectresponse');
var MIPProfileResponse = require(libs + 'model/mipprofileresponse');
var RoutingObject = require(libs + 'model/routingobject');
var RoutingObjectResponse = require(libs + 'model/routingobjectresponse');
var PrivacyObject = require(libs + 'model/privacyobject');
var OwnerObject = require(libs + 'model/ownerobject');
var TimeObject = require(libs + 'model/timeobject');

var TimeObjectResponse = require(libs + 'model/timeobjectresponse');

var AccessToken = require(libs + 'model/accessToken');
router.use(passport.initialize());


function createOwnnerAdmin(member_ids, mip_id, callback){
    var user_ids = [];
    asyncLoop(member_ids, function (item, next)
    {
        var owner_object = new OwnerObject ({
            user_id: item,
            target_id: mip_id,
            target_class: "mip_admin",
            time: Date.now()
        });
        //console.log(likes[i]);
        owner_object.save(function(err,owner_object){
            if(err)
            {
                next(err);
                return;
            }
            else
            {
                user_ids.push(owner_object.user_id);
                next();
            }

        });
    },

    function (err)
    {
        if (err)
        {
            callback(err)
        }
        else
            callback(null, user_ids);
    });
}
function checkOwnnerAdmin(member_ids, mip_id, callback)
{
    //console.log('tag--->'+member_ids);
    OwnerObject.find({target_id:mip_id, target_class:'mip_admin', user_id : { $in : member_ids }},function(err, owner_objects){
        if (err)
            callback(err);
        else
        {
            for (var i = 0; i < owner_objects.length; i++)
                member_ids = _.without(member_ids, owner_objects[i].user_id);
            if (member_ids.length == 0)
                callback(null, []);
            else
            {
                createOwnnerAdmin(member_ids, mip_id, function(err, list_ids){
                    if (err)
                        callback(err);
                    else {
                        callback(null, list_ids)
                    }
                });
            }
        }
    });
}

function create_routing_object(address_location, callback){
    var routing_object = new RoutingObject();

    utils.createTimeObject(function (err, timeobject) {
        if (err) {
            callback(err);
        }
        else
        {
            routing_object.time = timeobject._id;
            utils.createLocationObject(address_location.lat, address_location.long, function (err, locationobject) {
                if (err) {
                    callback(err);
                }
                else
                {
                    routing_object.location = locationobject._id;
                    routing_object.save(function (err, routingobject) {
                        if (err) {
                            callback(err);
                        }
                        else
                        {
                            callback(null, routingobject);
                        }
                    });
                }
            });
        }
    });
}
router.post('/create_bot', function(req,res){
    var mip_raw_object = new MipRawObject();
    mip_raw_object.title = req.body.title;
    mip_raw_object.address = req.body.address;
    mip_raw_object.itsopen = {"from":req.body.open,"to":req.body.close};
    mip_raw_object.location = {"lat":req.body.lat,"long":req.body.long};
    mip_raw_object.category = req.body.category;
    mip_raw_object.save(function(err,mip_raw_object){
        if (err)
            res.json({
                error: err.message
            });
        else
        {
            res.json({
                message: 'ok'
            });
        }
    });
});
function getMemberInfoByID(user_id, callback)
{
    var jsonobject={};
    //console.log(user_id);
    User.findOne({_id:user_id},function(err, user_object){
        if (err)
            callback(err);
        else if (user_object)
        {
            //console.log(user_object.profile);
            UserProfileObject.findOne({_id:user_object.profile},function(err, user_profile_object){
                if (err)
                    callback(err);
                else if (user_profile_object)
                {
                    //console.log(user_profile_object);
                    jsonobject._id = user_id;
                    jsonobject.username = user_profile_object.username;
                    jsonobject.display_name = user_profile_object.display_name;
                    jsonobject.avatar = user_profile_object.avatar;
                    callback(null, jsonobject);
                }
                else
                    callback(null, {});
            });
        }
    });
}
function getMipAdmins(members, callback){

    //console.log(members);
    var length = members.length;
    //console.log(length);
    var members_list = [];
    if (length==0)
        callback(null,[]);
    else
        asyncLoop(members, function (item, next)
            {
                getMemberInfoByID(item,function(err,members){
                    if(err)
                    {
                        next(err);
                        return;
                    }
                    else if (members)
                    {
                        members_list.push(members);
                        next();
                    }
                });
            },

            function (err)
            {
                if (err)
                {
                    callback(err)
                }
                else
                {
                    var members_list_result = [];
                    for (j = 0; j < members_list.length; j++)
                    {
                        if (members_list[j].username)
                            members_list_result.push(members_list[j]);
                    }
                    callback(null, members_list_result);
                }
            });
}
function get_Routing_Object_By_ID(routing_object_id, callback){
    var json_object = new RoutingObjectResponse();
    json_object._id = routing_object_id;
    RoutingObject.findOne({_id:routing_object_id},function(err, routing_object){
        if (err)
            callback(err);
        else{
            utils.getLocationObjectByID(routing_object.location, function(err, location_object){
                if (err)
                    callback(err);
                else
                json_object.location = location_object;
                utils.getTimeObjectByID(routing_object.time, function(err, time_object){
                    if (err)
                        callback(err);
                    else {
                        json_object.time = time_object;
                        callback(null, json_object);
                    }
                });

            });
        }
    });
}
function get_MIP_Profile_By_ID(mip_profile_id, callback){
    var json_object = new MIPProfileResponse();
    json_object._id = mip_profile_id;
    MIPProfileObject.findOne({_id:mip_profile_id},function(err, mip_profile_object){
        if (err)
            callback(err);
        else{
            json_object.address = mip_profile_object.address;
            json_object.title = mip_profile_object.title;
            get_Routing_Object_By_ID(mip_profile_object.address_location, function(err, routing_object){
                if (err)
                    callback(err);
                else {
                    json_object.address_location = routing_object;
                    utils.getBodyObjectByID(mip_profile_object.content, function(err, body_object){
                        if (err)
                            callback(err);
                        else {
                            json_object.content = body_object;
                            callback(null, json_object);
                        }
                    });

                }
            });
        }
    });
};
function get_MIP_Object_By_ID(mip_object_id, callback){
    var jsonObject = new MIPObjectResponse();
    jsonObject._id = mip_object_id;
    MIPObject.findOne({_id:mip_object_id},function(err, mip_object){
        if (err)
            callback(err);
        else if (mip_object)
        {
            //console.log('-->'+mip_object);
            utils.getTimeObjectByID(mip_object.time, function(err,time_object){
                if (err)
                    callback(err)
                else if (time_object)
                {
                    jsonObject.time = utils.getmilisecondtime(time_object);
                    //console.log(jsonObject);
                    utils.getPrivacyObjectByID(mip_object.privacy, function(err, privacy_object){
                        if (err)
                            callback(err)
                        else
                        {
                            if (privacy_object)
                            {
                                var json_privacy = {
                                    stricts:privacy_object.stricts,
                                    enable: privacy_object.enable,
                                    whitelist:privacy_object.whitelist,
                                    blacklist:privacy_object.blacklist,
                                    time: privacy_object.time,
                                    location: privacy_object.location
                                }
                                jsonObject.privacy = json_privacy;
                                //console.log('-->'+jsonObject);
                                utils.getUserProfileByID(mip_object.owner, function(err,user_profile){
                                    if (err)
                                        callback(err)
                                    else{
                                        //console.log('>>>'+user_profile);
                                        var json = {
                                            username:user_profile.username,
                                            display_name: user_profile.display_name,
                                            email: user_profile.email,
                                            display_name: user_profile.email,
                                            avatar: user_profile.avatar,
                                            formatted_address: user_profile.formatted_address,
                                            phonenumber: user_profile.phonenumber,
                                            gender: user_profile.gender,
                                            location: user_profile.location
                                        }
                                        jsonObject.owner = json;
                                        getMipAdmins(mip_object.admins, function(err, admins){
                                            if (err)
                                                callback(err)
                                            else
                                            {
                                                jsonObject.admins = admins;
                                                get_MIP_Profile_By_ID(mip_object.mip_profile, function(err, mip_profile){
                                                    jsonObject.mip_profile = mip_profile;

                                                    //console.log(jsonObject);
                                                    callback(null, jsonObject);

                                                });
                                            }


                                        });
                                    }
                                });
                            }
                            else
                            {
                                var err = {
                                    message:'no privacy object found'
                                }

                                callback(err);
                            }
                        }
                    });
                }
            });
        }
        else
        {
            var err = {
                message:'no mip object found'
            }

            callback(err);
        }
    });
}

function getAllMipProfileObjectID(ids, callback){
    MIPProfileObject.find( { address_location : { $in : ids } },{ _id : 1 }, function(err, result){
        if (err)
        {
            console.log(err);
            callback(err);
        }

        else
            callback(null, result);
    });
}
function getAllMipObjectID(ids, callback){
    MIPObject.find( { mip_profile : { $in : ids } },{ _id : 1 }, function(err, result){
        if (err)
            callback(err);
        else
            callback(null, result);
    });
}
function getAllMipObject(ids, callback){
    var mip_objects = [];
    asyncLoop(ids, function (item, next)
    {
        get_MIP_Object_By_ID(item._id,function (err, mip_object){
            if (err)
            {
                next(err);
                return;
            }
            else {
                mip_objects.push(mip_object);
                next();
            }
        });
    }, function (err)
    {
        if (err)
        {
            callback(err)
        }
        else
            callback(null, mip_objects)
    });
}
router.get('/', function (req, res) {
    LocationObject.find({ coordinates :
    { $geoWithin :{ $box : [ [ req.headers.left , req.headers.bottom ] , [ req.headers.right , req.headers.top ] ]} }
    },function(err,locationlbject){
        if(err){
            //console.log('error');
            res.json({
                err: err.message
            });
        }
        else {
            if (locationlbject.length==0){
                //console.log('not found');
                res.json({
                    message: 'not found'
                });
            }
            else {

                var _ids = [];
                var length = locationlbject.length;

                for (var j = 0; j < length; j++)
                    _ids.push(locationlbject[j]._id);
                RoutingObject.find( { location : { $in : _ids } },{ _id : 1 }, function(err, result){
                    if (err)
                        res.json({
                            message : err.message
                        });
                    else if (result)
                    {
                        console.log(result.length + ' routing(s) found');
                        if (result.length > 0)
                        {
                            var __ids = [];

                            for (var k = 0; k < result.length; k++)
                            {
                                //console.log('--->'+result[k]);
                                __ids.push(result[k]._id);
                            }

                            getAllMipProfileObjectID(__ids, function (err, mip_profiles){
                                if (err)
                                    res.json({
                                        message : err.message
                                    });
                                else if (mip_profiles.length > 0)
                                {
                                    var ___ids = [];
                                    for (var l = 0; l < mip_profiles.length; l++)
                                        ___ids.push(mip_profiles[l]._id);
                                    getAllMipObjectID(___ids, function (err, mip_ids){
                                        if (err)
                                            res.json({
                                                message : err.message
                                            });
                                        else if (mip_ids.length > 0)
                                        {
                                            getAllMipObject(mip_ids, function (err, mips){
                                                if (err)
                                                    res.json({
                                                        message : err.message
                                                    });
                                                else
                                                    res.json({
                                                        mips : mips
                                                    });
                                            });
                                        }
                                        else
                                            res.json({
                                                mips : []
                                            });
                                    });
                                }
                                else
                                    res.json({
                                        mips : []
                                    });
                            });
                        }

                        else
                            res.json({
                                mips : []
                            });
                    }
                    else
                        res.json({
                            mips : []
                        });
                });

            }
        }
    });

});

router.get('/id', function(req,res){
    MIPObject.findOne( { _id : req.headers._id}, function(err, mip_object){
        if (err)
            res.json({
                error: err.message
            });
        else if (!mip_object)
            res.json({
                error: 'no MIP object found'
            });
        else {
            get_MIP_Object_By_ID(req.headers._id,function (err, mip_object){
                if (err)
                    res.json({
                        error: err.message
                    });
                else {
                    res.json({
                        mip_object: mip_object
                    });
                }
            });
        }
    });
});
router.post('/', passport.authenticate('bearer', { session: false }), function(req,res){
    var check = true;
    if (!req.body.hasOwnProperty("mip"))
        check = false;
    else
    {
        if (!req.body.mip.hasOwnProperty("privacy_object"))
            check = false;
        else if (!req.body.mip.hasOwnProperty("admins"))
            check = false;
        else if (!req.body.mip.hasOwnProperty("mip_profile"))
            check = false;
        else if (!req.body.mip.hasOwnProperty("linked_mips"))
            check = false;
        else if (!req.body.mip.mip_profile.hasOwnProperty("address_location"))
            check = false;
        else if (!req.body.mip.mip_profile.hasOwnProperty("content"))
            check = false;
    }
    //console.log(check);
    if (!check)
        res.json({
            error: 'error: bad data'
        })
    else
    {
        var mipobject = new MIPObject();
        mipobject.owner = req.user.userId;
        var privacyobject = new PrivacyObject();
        privacyobject.stricts = req.body.mip.privacy_object.stricts;
        if (req.body.mip.privacy_object.whitelist)
            privacyobject.whitelist = req.body.mip.privacy_object.whitelist;
        if (req.body.mip.privacy_object.blacklist)
            privacyobject.blacklist = req.body.mip.privacy_object.blacklist;
        utils.createPrivacyObject(privacyobject, function (err, privacyobject) {
            if (err) {
                res.json({
                    error: err.message
                });
            }
            else {
                mipobject.privacy = privacyobject._id;
                var mip_profile = new MIPProfileObject();
                mip_profile.title = req.body.mip.mip_profile.title;
                mip_profile.address = req.body.mip.mip_profile.address;
                mip_profile.address_location = req.body.mip.mip_profile.address_location;
                mip_profile.url = req.body.mip.mip_profile.url;
                utils.createTextObject(null, req.body.mip.mip_profile.content.text_object.content,req.body.mip.mip_profile.content.text_object.index, function(err, text_object){
                    if (err) {
                        res.json({
                            error: err.message
                        });
                    }
                    else if(text_object){
                        var bodyobject = new BodyObject({
                            text: text_object._id
                        });
                        if (req.body.mip.mip_profile.content.videos.length > 0){
                            bodyobject.videos = req.body.mip.mip_profile.content.videos;
                        }
                        if (req.body.mip.mip_profile.content.images){
                            bodyobject.images = req.body.mip.mip_profile.content.images;
                        }

                        utils.createBodyObject(bodyobject, function (err, bodyobject) {
                            if (err) {
                                res.json({
                                    error: err.message
                                });
                            }
                            else if (bodyobject){
                                mip_profile.content = bodyobject._id;
                                mip_profile.save(function(err, mip_profile){
                                    if (err) {
                                        res.json({
                                            error: err.message
                                        });
                                    }
                                    else if(mip_profile){
                                        mipobject.mip_profile = mip_profile._id;
                                        utils.createTimeObject(function (err, timeobject) {
                                            if (err)
                                                res.json({
                                                    error: err.message
                                                });
                                            else {
                                                mipobject.time = timeobject._id;

                                                mipobject.save(function(err, mipobject){
                                                    if (err) {
                                                        res.json({
                                                            error: err.message
                                                        });
                                                    }
                                                    else if(mipobject){
                                                        var unique_admins = req.body.mip.admins.filter(function(elem, index, self) {
                                                            return index == self.indexOf(elem);
                                                        })
                                                        checkOwnnerAdmin(unique_admins, mipobject._id, function(err, admins){
                                                            if (admins)
                                                            {
                                                                //console.log(members);
                                                                MIPObject.update({_id:mipobject._id},
                                                                    { $addToSet: {admins: { $each:admins} }

                                                                    },function (err, number) {
                                                                        if (err)
                                                                        {
                                                                            res.json({
                                                                                error: 'update mip object fail'
                                                                            });
                                                                        }
                                                                        else
                                                                        {
                                                                            //console.log(number);
                                                                            var owner_object = new OwnerObject ({
                                                                                user_id: req.user.userId,
                                                                                target_id: mipobject._id,
                                                                                target_class: "mip_owner",
                                                                                time: Date.now()
                                                                            });
                                                                            //console.log(likes[i]);
                                                                            owner_object.save(function(err,owner_object){
                                                                                if (err)
                                                                                    res.json({
                                                                                        error: err.message
                                                                                    });
                                                                                else if (owner_object)
                                                                                {
                                                                                    res.json({
                                                                                        mipobject_id: mipobject._id
                                                                                    });

                                                                                }
                                                                            });

                                                                        }
                                                                    }
                                                                )
                                                            }
                                                        });

                                                    }
                                                });
                                            }
                                        });

                                    }
                                });
                            }
                        });
                    }
                });


            }
        });
    }
});
router.post('/create_routing', passport.authenticate('bearer', { session: false }), function (req, res) {
    create_routing_object(req.body.location, function(err, routing_object){
        if (err)
            res.json({
                error: err.message
            });
        else
        {
            res.json({
                routing_object_id: routing_object._id
            });
        }
    })
});
router.put('/update_routing', passport.authenticate('bearer', { session: false }), function (req, res) {
    RoutingObject.findOne({_id:req.headers.id},function(err, routing_object){
        if (err)
            res.json({
                error: err.message
            });
        else{
            if (!routing_object)
                res.json({
                    message: 'no routing_object found'
                });
            else
                LocationObject.update({_id: routing_object.location},{lat: req.body.location.lat, long: req.body.location.long}, function(err, number){
                    if (err)
                        res.json({
                            error: err.message
                        });
                    else
                    {
                        var now = Date.now();
                        TimeObject.update({_id: routing_object.time},{lastmodified_date: now}, function(err, number){
                            if (err)
                                res.json({
                                    error: err.message
                                });
                            else
                            {
                                res.json({
                                    message: 'updated'
                                });
                            }

                        });
                    }

                });
        }
    });


});
module.exports = router;