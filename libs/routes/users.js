//../api/users
var express = require('express');
var fs = require('fs');
var helper = require('sendgrid').mail;
var passport = require('passport');
var moment = require('moment');
var timediff = require('timediff');
var distance = require('gps-distance');
var router = express.Router();
var libs = process.cwd() + '/libs/';
var config = require(libs + 'config');
var host = config.get('host')
var port = config.get('port')

var libs = process.cwd() + '/libs/';
var db = require(libs + 'db/mongoose');
var User = require(libs + 'model/user');

var friends = require("mongoose-friends");
var AccessToken = require(libs + 'model/accessToken');
var RefreshToken = require(libs + 'model/refreshToken');
var UserProfileObject = require(libs + 'model/userprofileobject');
var Status = friends.Status;
var from_email = new helper.Email(config.get('from_email'));

var subject = 'Activate '+config.get('app_name')+' account';

router.use(passport.initialize());
var sg = require('sendgrid')(process.env.SENDGRID_API_KEY);

var crypto, multer, path, storage;

multer = require('multer');

path = require('path');

crypto = require('crypto');

router.post('/updatefcmtoken', function (req, res) {
    User.findOne({ username: req.body.username }, function (err, user) {
        if (err) {
            res.json({
                message: err.message,
                errmsg: err.errmsg
            })
        }
        else
        {
            if (!user) {
                res.json({
                    message: "user not found"
                })
            }
            else {
					User.update({ _id: user._id }, { fcmtoken: req.body.fcmtoken }, function (error_, numberAffected) {
                    if (error_)
                        res.json({
                            message: error_
                        });
                    else {
						res.json({
                            message: "OK"
                        });
					}
					});
			}
		}
	});
});
router.get('/getprofile', passport.authenticate('bearer', { session: false }), function(req,res){
    User.findOne({ _id: req.user.userId },{_id: 0,profile:1}, function (err, user) {
        if (err) {
            res.json({
                err: err.message
            })
        }
        else {
            if (!user) {
                res.json({
                    err: "User not found"
                })
            }
            else {
                UserProfileObject.findOne({ _id: user.profile },{category_packages:0}, function (err, profile) {
                    if (err) {
                        res.json({
                            err: err.message
                        })
                    }
                    else {
                        if (!profile) {
                            res.json({
                                err: "profile not found"
                            })
                        }
                        else{
                            res.json({
                                profile: profile
                            })
                        }
                    }
                });
            }
        }
    });
});
function SetRelationShip(user_id, friend_id, callback){

}
router.post('/relation', passport.authenticate('bearer', { session: false }), function(req,res){
    User.findOne({ _id: req.user.userId },{_id: 0,profile:1}, function (err, user) {
        if (err) {
            res.json({
                err: err.message
            })
        }
        else {
            if (!user) {
                res.json({
                    err: "User not found"
                })
            }
            else {
                if (req.target.action == 'relation_set')
                {
                    SetRelationShip(req.user.userId,req.body.target._id, function(err, result){
                        if (err)
                            res.json({
                                err: err.message
                            })
                        else
                            res.json({
                                message: result
                            })
                    });
                }
            }
        }
    });
});
router.post('/', function (req, res) {
    var now = Date.now();
	var userprofileobject = new UserProfileObject({
		username: req.body.username,
        email: req.body.email
	});
	userprofileobject.save(function(err,userprofileobject){
		if (err) {//telephone_1 dup
						if (err.message.indexOf(" username_1 dup") !=-1)
						res.json({
							message: "Username already registed"
						})
                        else if (err.message.indexOf(" email_1 dup") !=-1)
                            res.json({
                                message: "Email already registed"
                            })
						else
							res.json({
							message: err.message
						})
					}
		else{
			if (!userprofileobject)
			{
				res.json({
                    err: 'save profile error'
				});
			}
			else
			{
				var user = new User({
					username: req.body.username,
					password: req.body.password,
                    created: now,
                    lastlogindate: now,
                    emaillinkdate: now,
					profile: userprofileobject._id
				});
				user.save(function (err, user) {
					if (err) {
						if (err.message.indexOf(" username_1 dup") !=-1)
						res.json({
							message: "Username already registed"
						})
						else
							res.json({
							err: err.message
						})
					}
					else 
					{
						if (!user)
						{
							res.json({
								err: 'save user error'
							});
						}
						else
						{
							res.json({
								message: 'success',
								user_id: user._id
							});
						}
					}
				});
			}
		}
	});
});


module.exports = router;