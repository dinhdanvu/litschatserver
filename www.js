#!/usr/bin/env node
var imagemagick = require('imagemagick-stream');
var express = require('express');

var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var asyncLoop = require('node-async-loop');
var debug = require('debug')('restapi');
var libs = process.cwd() + '/libs/';
var config = require(libs + 'config');
var log = require(libs + 'log')(module);
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
var multer = require('multer');
var HashMap = require('hashmap');
var colors = require('colors/safe');
var _ = require('underscore');
var methodOverride = require('method-override');

require(libs + 'auth/auth');

var oauth2 = require(libs + 'auth/oauth2');
var users = require(libs +'routes/users');
var mips = require(libs +'routes/mips');
var journals = require(libs +'routes/journals');
var chatmessages = require(libs +'routes/chatmessages');

var missions = require(libs +'routes/missions');
var utils = require(libs + 'utils');
var db = require(libs + 'db/mongoose');

app.use(express.static(__dirname+'/startbootstrap'));

app.use(cookieParser());
app.use(methodOverride());
app.use(passport.initialize());

app.use('/api/v1/users', users);
app.use('/api/v1/missions', missions);
app.use('/api/v1/mips', mips);
app.use('/api/v1/journals', journals);
app.use('/api/v1/chatmessages', chatmessages);
app.use('/api/v1/oauth/token', oauth2.token);

var StatusPackage = require(libs + 'model/statuspackage');
var StatusPackageResponse = require(libs + 'model/statuspackageresponse');
var StatusObjectResponse = require(libs + 'model/statusobjectresponse');
var TextObjectResponse = require(libs + 'model/textobjectresponse');
var VideoObjectResponse = require(libs + 'model/videoobjectresponse');
var ImageObjectResponse = require(libs + 'model/imageobjectresponse');
var BodyObjectResponse = require(libs + 'model/bodyobjectresponse');
var CommentObjectResponse = require(libs + 'model/commentobjectresponse');
var LikeObjectResponse= require(libs + 'model/likeobjectresponse');
var StatusObject = require(libs + 'model/statusobject');
var LocationObject = require(libs + 'model/locationobject');
var MileStoneObject = require(libs + 'model/milestoneobject');
var TimeObject = require(libs + 'model/timeobject');
var TimeObjectResponse = require(libs + 'model/timeobjectresponse');
var UserObjectResponse = require(libs + 'model/userobjectresponse');
var VideoObject = require(libs + 'model/videoobject');
var ImageObject = require(libs + 'model/imageobject');
var PrivacyObject = require(libs + 'model/privacyobject');
var PrivacyObjectResponse = require(libs + 'model/privacyobjectresponse');
var BodyObject = require(libs + 'model/bodyobject');
var TextObject = require(libs + 'model/textobject');
var CommentContentObject = require(libs + 'model/commentcontentobject');
var CommentObject = require(libs + 'model/commentobject');
var LikeObject = require(libs + 'model/likeobject');
var OwnerObject = require(libs + 'model/ownerobject');
var InteractionObject = require(libs + 'model/interactionobject');
var UserProfileObject = require(libs + 'model/userprofileobject');
var ChatMessage = require(libs + 'model/chatmessage');
var ChatPackage = require(libs + 'model/chatpackage');
var ChatObject = require(libs +'model/chatobject');
var MissionPackage = require(libs + 'model/missionpackage');
var User = require(libs + 'model/user');
var AccessToken = require(libs + 'model/accessToken');
var geocoder = require('geocoder');
var distance = require('gps-distance');//gps
var request = require('request');
var host = config.get('host')

var storage_pictures = multer.diskStorage({
    destination: './startbootstrap/uploads/pictures',
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
});
var storage_avatar = multer.diskStorage({destination: './startbootstrap/uploads/avatar/big',
    filename: function (req, file, cb) {
        console.log(file);
        cb(null, file.originalname)
    }
});
var storage_videos = multer.diskStorage({
    destination: './startbootstrap/uploads/videos',
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
});

function createCommentObject(location, commentobject, callback) {


    utils.createTimeObject(function (err, timeobject) {
        if (err)
            callback(err);
        else {
            if (location)
            {
                utils.createLocationObject(location.lat,location.long, function (err, locationobject) {
                    if (err)
                        callback(err);
                    else {
                        var comment_object = new CommentObject();
                        comment_object.content = commentobject.content;
                        comment_object.time = timeobject._id;
                        comment_object.user_profile = commentobject.user_profile;
                        comment_object.location = locationobject._id;
                        comment_object.save(function (err, comment_object) {
                            if (err)
                                callback(err);
                            else {
                                callback(null, comment_object);
                            }
                        });
                    }
                });
            }
            else
            {
                var comment_object = new CommentObject();
                comment_object.content = commentobject.content;
                comment_object.time = timeobject._id;
                comment_object.user_profile = commentobject.user_profile;
                comment_object.save(function (err, comment_object) {
                    if (err)
                        callback(err);
                    else {
                        callback(null, comment_object);
                    }
                });
            }
        }
    });

}
function isLiked_status_package(user_id,package_id, callback){
    //console.log(user_id);
    OwnerObject.findOne({user_id:user_id, target_id:package_id, target_class:'like_status_package'},function(err, owner_object){
        if (err)
            callback(err.message);
        else if (owner_object)
        {
            callback(null,'y');
        }
        else
            callback(null,'n');
    });
}

function insertCommentObjectToStatusPackage(comment_object_id, target_id, callback){

    StatusPackage.findOne({_id:target_id},function(err, status_package){
        if (err)
            callback(err);
        else if (status_package)
        {
            //console.log('sds');
            InteractionObject.update({_id:status_package.interaction},
                {$push: {
                    "comments":comment_object_id
                }
                },function (err, number) {
                    if (err)
                    {

                        callback(err);
                    }

                    else
                        callback(null, number);
                }
            )
        }
    });
}
function insertLikeObjectToStatusPackage(like_object_id, target_id, callback){
    console.log(target_id.toString());
    StatusPackage.findOne({_id:target_id.toString()},function(err, status_package){
        if (err)
        {
            //console.log(err);
            callback(err);
        }
        if (!status_package){
            //console.log('not found');
        }
        else if (status_package)
        {

            InteractionObject.update({_id:status_package.interaction},
                {$push: {
                    "likes":like_object_id
                }
                },function (err, number) {
                    if (err)
                    {

                        callback(err);
                    }

                    else
                        callback(null, number);
                }
            )
        }
    });
}
function createCommentContentObject(commentcontentobject,callback) {
    utils.createTimeObject(function (err, timeobject) {
        if (err)
            callback(err);
        else {
            var comment_content_object = new CommentContentObject();
            comment_content_object.content = commentcontentobject.content;
            comment_content_object.time = timeobject._id
            comment_content_object.save(function (err, comment_content_object) {
                if (err)
                    callback(err);
                else {
                    callback(null, comment_content_object);
                }
            });
        }
    });
}

function createInteractionObject(callback) {
    utils.createTimeObject(function (err, timeobject) {
        if (err)
            callback(err);
        else {
            var interaction_object = new InteractionObject();
            interaction_object.time = timeobject._id;
            interaction_object.save(function (err, interaction_object) {
                if (err)
                    callback(err);
                else
                    callback(null,interaction_object);
            });

        }
    });
}
function createStatusObject(statusobject,callback) {
    if (statusobject.location_object)
    {
        utils.createLocationObject(statusobject.location_object.lat,statusobject.location_object.long,function (err, locationobject) {
            if (err)
                callback(err);
            else {
                utils.createTimeObject(function (err, timeobject) {
                    if (err)
                        callback(err);
                    else {
                        var status_object = new StatusObject();
                        status_object.content = statusobject.content;
                        status_object.embed = statusobject.embed;
                        status_object.categories = statusobject.categories;
                        status_object.time = timeobject._id
                        status_object.location = locationobject._id
                        status_object.save(function (err, status_object) {
                            if (err)
                                callback(err);
                            else {
                                callback(null, status_object);
                            }
                        });
                    }
                });
            }
        });
    }
    else
    {
        utils.createTimeObject(function (err, timeobject) {
            if (err)
                callback(err);
            else {
                var status_object = new StatusObject();
                status_object.content = statusobject.content;
                status_object.embed = statusobject.embed;
                status_object.categories = statusobject.categories;
                status_object.time = timeobject._id
                status_object.save(function (err, status_object) {
                    if (err)
                        callback(err);
                    else {
                        callback(null, status_object);
                    }
                });
            }
        });
    }
}
function createPrivacyObject(privacyobject,callback) {
    if (privacyobject.location_object)
    {
        utils.createLocationObject(privacyobject.location_object.lat, privacyobject.location_object.long, function (err, locationobject) {
            if (err)
                callback(err);
            else {
                utils.createTimeObject(function (err, timeobject) {
                    if (err)
                        callback(err);
                    else {
                        var privacy_object = new PrivacyObject();
                        privacy_object.stricts = privacyobject.stricts;
                        privacy_object.whitelist = privacyobject.whitelist;
                        privacy_object.blacklist = privacyobject.blacklist;
                        privacy_object.enable = privacyobject.enable;
                        privacy_object.time = timeobject._id
                        privacy_object.location = locationobject._id
                        privacy_object.save(function (err, privacy_object) {
                            if (err)
                                callback(err);
                            else {
                                callback(null, privacy_object);
                            }
                        });
                    }
                });
            }
        });
    }
    else
    {
        utils.createTimeObject(function (err, timeobject) {
            if (err)
                callback(err);
            else {
                var privacy_object = new PrivacyObject();
                privacy_object.stricts = privacyobject.stricts;
                privacy_object.whitelist = privacyobject.whitelist;
                privacy_object.blacklist = privacyobject.blacklist;
                privacy_object.enable = privacyobject.enable;
                privacy_object.time = timeobject._id
                privacy_object.save(function (err, privacy_object) {
                    if (err)
                        callback(err);
                    else {
                        callback(null, privacy_object);
                    }
                });
            }
        });
    }
}
function createStatusPackage(statuspackage,callback) {
    if (statuspackage.location_object)
    {
        utils.createLocationObject(statuspackage.location_object.lat, statuspackage.location_object.long, function (err, locationobject) {
            if (err)
                callback(err);
            else {
                utils.createTimeObject(function (err, timeobject) {
                    if (err)
                        callback(err);
                    else {
                        var status_package = new StatusPackage();
                        status_package.privacy = statuspackage.privacy;
                        status_package.user_profile = statuspackage.user_profile;
                        status_package.content = statuspackage.content;
                        status_package.time = timeobject._id;
                        status_package.location = locationobject._id;
                        status_package.interaction = statuspackage.interaction;
                        status_package.save(function (err, status_package) {
                            if (err)
                                callback(err);
                            else {
                                callback(null, status_package);
                            }
                        });
                    }
                });
            }
        });
    }
    else
    {
        utils.createTimeObject(function (err, timeobject) {
            if (err)
                callback(err);
            else {
                var status_package = new StatusPackage();
                status_package.privacy = statuspackage.privacy;
                status_package.content = statuspackage.content;
                status_package.time = timeobject._id
                status_package.save(function (err, status_package) {
                    if (err)
                        callback(err);
                    else {
                        callback(null, status_package);
                    }
                });
            }
        });
    }
}

function getCommentContentObjectByID(comment_content_id, callback){
    var jsonobject = new CommentContentObject();
    jsonobject._id = comment_content_id;
    CommentContentObject.findOne({_id: comment_content_id}, function(err, comment_content_object){
        if (err)
            callback(err);
        else if (comment_content_object){
            //console.log('--content----+++++>');
            getBodyObjectByID(comment_content_object.content, function(err, body_object){
                if (err)
                    callback(err);
                else if (body_object){
                    //console.log('--body----+++++>');
                    jsonobject.content = body_object;
                    callback(null, jsonobject);
                }
            });
        }
    });
}

function getUserInteractionInfoByID(user_id, callback)
{
    var jsonobject = new UserObjectResponse();
    jsonobject._id = user_id;
    //console.log('--user_object----+++++>'+user_id);
    User.findOne({_id:user_id},function(err, user_object){
        if (err)
            callback(err);
        else if (user_object)
        {

            UserProfileObject.findOne({_id:user_object.profile},function(err, user_profile_object){
                if (err)
                    callback(err);
                else if (user_profile_object)
                {
                    jsonobject.avatar = user_profile_object.avatar;
                    jsonobject.username = user_profile_object.username;
                    jsonobject.display_name = user_profile_object.display_name;
                    callback(null, jsonobject);
                }
            });
        }
    });
}
function getComment(comments_id, callback){
    //console.log(comments_id);
    var jsonobject = new CommentObjectResponse();
    jsonobject._id = comments_id;
    CommentObject.findOne({_id:comments_id},function(err, comment_object){
        if (err)
            callback(err);
        else if (comment_object)
        {
            //console.log('body_object here--->'+body_object);
            utils.getTimeObjectByID(comment_object.time, function(err, time_object){
                if (err)
                    callback(err);
                else if (time_object)
                {
                    jsonobject.time = time_object;
                    utils.getLocationObjectByID(comment_object.location, function(err, location_object){
                        if (err)
                            callback(err);
                        else if (location_object){
                            //console.log('--local----+++++>');
                            jsonobject.location = location_object;
                            getCommentContentObjectByID(comment_object.content, function(err, comment_content_object){
                                if (err)
                                    callback(err);
                                else if (comment_content_object){
                                    //console.log('--comment_content_object----+++++>'+comment_object);
                                    jsonobject.content = comment_content_object;

                                    getUserInteractionInfoByID(comment_object.user_profile, function(err, user_interaction_info){
                                        if (err)
                                            callback(err);
                                        else if (user_interaction_info){

                                            jsonobject.user_interaction_info = user_interaction_info;
                                            //console.log(jsonobject);
                                            callback(null, jsonobject);
                                        }
                                    })
                                }
                            });
                        }
                        else
                        {
                            getCommentContentObjectByID(comment_object.content, function(err, comment_content_object){
                                if (err)
                                    callback(err);
                                else if (comment_content_object){
                                    //console.log('--comment_content_object----+++++>');
                                    jsonobject.content = comment_content_object;
                                    getUserInteractionInfoByID(comment_object.user_profile, function(err, user_interaction_info){
                                        if (err)
                                            callback(err);
                                        else if (user_interaction_info){
                                            //console.log('--comment_content_object----+++++>');
                                            jsonobject.user_interaction_info = user_interaction_info;
                                            callback(null, jsonobject);
                                        }
                                    })
                                }
                            });
                        }
                    });
                }
            });
        }
    });

}

function getAllComment(comments,start,limit, callback){
    var length = comments.length;
    var end = length;
    var comments_result = [];
    //console.log('length:'+length+';start'+start+';end:'+end);
    if (start>=0 && limit>0)
    {
        if (start>=length)
            callback(null, []);
        else{
            if (limit+start>length)
                end = length;
            else
                end = start + limit;

            asyncLoop(comments, start, end -1 , function (item, next)
            {
                getComment(item,function (err, comment){
                    if(err)
                    {
                        next(err);
                        return;
                    }
                    else
                    {
                        comments_result.push(comment);
                        next();
                    }
                });

            }, function (err)
            {
                if (err)
                {
                    callback(err)
                }
                else
                    callback(null, comments_result);
            });
        }
    }
    else
    {
        var err = {
            message:'bad range limit'
        }

        callback(err);
    }

}
function getLikeUser(owner, callback){
    //console.log(owner.user_id);
    User.findOne({_id:owner.user_id},function(err, user){
        if (err)
            callback(err);
        else
        {
            callback(null, user.username);
        }
    });
}
function getAllUserLike(owners,start,limit, callback){
    var length = owners.length;
    var end = length;
    var users_like_result = [];
    if (start>=0 && limit>0)
    {
        if (start>=length)
            callback(null, []);
        else {
            if (limit+start>length)
                end = length;
            else
                end = start + limit;
            //console.log('length:'+length+';start'+start+';end:'+end);

            asyncLoop(owners,start, end - 1, function (item, next)
            {
                getLikeUser(item, function (err, user){
                    if (err)
                    {
                        next(err);
                        return;
                    }
                    else
                    {
                        users_like_result.push(user);
                        next();
                    }
                });

            }, function (err)
            {
                if (err)
                {
                    callback(err)
                }
                else
                    callback(null, users_like_result);
            });
        }
    }
    else
    {
        var err = {
            message:'bad range limit'
        }

        callback(err);
    }

}

function getStatusObjectByID(status_object_id, callback){
    //console.log('find status_object here--->'+status_object_id);
    var jsonobject = new StatusObjectResponse();
    jsonobject._id = status_object_id;
    StatusObject.findOne({_id:status_object_id},function(err, status_object){
        if (err)
            callback(err);
        else if (status_object)
        {
            //console.log('status_object here--->');
            utils.getTimeObjectByID(status_object.time, function(err, time_object){
                if (err)
                    callback(err);
                else if (time_object)
                {

                    jsonobject.time = time_object;
                    if (status_object.location){

                        utils.getLocationObjectByID(status_object.location, function(err, location_object){
                            if (err)
                                callback(err);
                            else if (location_object){
                                jsonobject.location = location_object;
                                utils.getBodyObjectByID(status_object.content, function(err, body_object){
                                    if (err)
                                        callback(err);
                                    else if (body_object)
                                    {

                                        jsonobject.content = body_object;
                                        callback(null, jsonobject);
                                    }
                                });
                            }
                            else
                            {
                                utils.getBodyObjectByID(status_object.content, function(err, body_object){
                                    if (err)
                                        callback(err);
                                    else if (body_object)
                                    {

                                        jsonobject.content = body_object;
                                        callback(null, jsonobject);
                                    }
                                });
                            }
                        });
                    }
                    else{

                        utils.getBodyObjectByID(status_object.content, function(err, body_object){
                            if (err)
                                callback(err);
                            else if (body_object)
                            {

                                jsonobject.content = body_object;
                                callback(null, jsonobject);
                            }
                        });
                    }
                }
            });
        }
    });
}

function getStatusPackageByID(status_package_id, callback){

    var jsonObject = new StatusPackageResponse();
    //console.log(status_package_id);
    jsonObject._id = status_package_id;
    StatusPackage.findOne({_id:status_package_id},function(err, status_package){
        if (err)
            callback(err);
        else if (status_package)
        {
            //console.log('-->'+status_package);
            utils.getTimeObjectByID(status_package.time,function(err,time_object){
                if (err)
                    callback(err)
                else if (time_object)
                {
                    jsonObject.time = time_object;
                    //console.log(jsonObject);
                    utils.getLocationObjectByID(status_package.location,function(err,location_object){
                        if (err)
                        {
                            callback(err)
                        }
                        else if (location_object)
                        {
                            //console.log('location object here--->'+location_object);

                            jsonObject.location = location_object;
                            getStatusObjectByID(status_package.content, function(err, status_object){
                                if (err)
                                    callback(err)
                                else if (status_object)
                                {
                                    //console.log('status object here--->'+status_object._id);
                                    jsonObject.content = status_object;
                                    if (status_package.privacy){

                                        utils.getPrivacyObjectByID(status_package.privacy, function(err, privacy_object){
                                            if (err)
                                                callback(err)
                                            else
                                            {
                                                if (privacy_object)
                                                {
                                                    var json_privacy = {
                                                        stricts:privacy_object.stricts,
                                                        enable: privacy_object.enable,
                                                        whitelist:privacy_object.whitelist,
                                                        blacklist:privacy_object.blacklist,
                                                        time: privacy_object.time,
                                                        location: privacy_object.location
                                                    }
                                                    jsonObject.privacy = json_privacy;
                                                    if (status_package.user_profile)
                                                    {
                                                        utils.getUserProfileByID(status_package.user_profile, function(err,user_profile){
                                                            if (err)
                                                                callback(err)
                                                            else{
                                                                if (user_profile)
                                                                {
                                                                    //console.log('-->'+user_profile);
                                                                    var json = {
                                                                        username:user_profile.username,
                                                                        display_name: user_profile.display_name,
                                                                        email: user_profile.email,
                                                                        display_name: user_profile.email,
                                                                        avatar: user_profile.avatar,
                                                                        formatted_address: user_profile.formatted_address,
                                                                        phonenumber: user_profile.phonenumber,
                                                                        gender: user_profile.gender,
                                                                        location: user_profile.location
                                                                    }
                                                                    jsonObject.user_profile = json;
                                                                    utils.getInteractionObjectByID(status_package.interaction, function (err, interaction_object) {
                                                                        if (err)
                                                                            callback(err)
                                                                        else{
                                                                            jsonObject.like_count = interaction_object.like_count;
                                                                            jsonObject.comment_count = interaction_object.comment_count;
                                                                            callback(null, jsonObject);
                                                                        }
                                                                    });
                                                                    //console.log(jsonObject);

                                                                }
                                                                else
                                                                    callback(null, jsonObject);
                                                            }
                                                        });
                                                    }

                                                    else {
                                                        callback(null, jsonObject);
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    else{
                                        //console.log('status object here--->'+jsonObject);
                                        callback(null, jsonObject);
                                    }
                                }
                            });
                            //console.log('-->'+jsonObject);

                        }

                    });
                }
            });
        }
    });
}

function getAllStatusPackage(ids, callback){
    var statuspackages = [];
    asyncLoop(ids, function (item, next)
    {
        getStatusPackageByID(item._id,function (err, statuspackage){
            if (err)
            {
                next(err);
                return;
            }
            else if (statuspackage){

                statuspackages.push(statuspackage);
                next();
            }
            else
                next();
        });
    }, function (err)
    {
        if (err)
        {
            callback(err)
        }
        else
            callback(null, statuspackages)
    });
}
function sendMessageToUser(deviceId, message, title, callback) {
    console.log();
    request({
        url: 'https://fcm.googleapis.com/fcm/send',
        method: 'POST',
        headers: {
            'Content-Type' :' application/json',
            'Authorization': 'key=AAAAMBdyIbE:APA91bFyY3ueQNb9O06yFBnGfXmG4XoCgMScp6toEhZI8R3DklreFaGgXef8d6SbfqFJzk0TjZejTt_q5TygjZ0pttfkgJSlaAPEjWC_hHDMVn7BzrLETs9X-nNTYZpBl5Nrk8kUfPAj'
        },
        body: JSON.stringify(
            {
                "to" : deviceId,
                "notification" : {
                    "body" : message,
                    "title" : title,
                    "sound":"default"
                }
            }
        )
    }, function(error, response, body) {
        if (error) {
            callback(error);
        }
        else if (response.statusCode >= 400) {
            console.error('HTTP Error: '+response.statusCode+' - '+response.statusMessage+'\n'+body);
            callback({
                message:response.statusCode+' - '+response.statusMessage+':'+body
            });
        }
        else {
            callback(null,'OK');
        }
    });
}


app.use(function(err, req, res, next){
    res.status(err.status || 500);
    log.error('%s %d %s', req.method, res.statusCode, err.message);
    res.json({
        error: err.message
    });
    return;
});
app.get('/api/v1/milestone/id', passport.authenticate('bearer', { session: false }), function(req, res) {
    if (!req.headers.id)
        res.json({
            message: 'bad data'
        });
    else
    {
        utils.getMileStoneByID(req.headers.id, req.user.userId, function(err, milestone_object){
            if (err)
                res.json({
                    error: err.message
                });
            else
            {
                res.json({
                    milestone_object: milestone_object
                });
            }
        });
    }
});
app.post('/api/v1/uploadavatar', passport.authenticate('bearer', { session: false }), multer({
    storage: storage_avatar
}).single('upload'), function(req, res) {

    console.log(req.headers.filename);

    imagemagick('./startbootstrap/uploads/avatar/big/'+req.headers.filename)
        .resize(80, 80)
        .quality(100)
        .to('./startbootstrap/uploads/avatar/small/'+req.headers.filename);

    User.findOne({ _id: req.user.userId }, function (err, user) {
        if (err) {
            res.json({
                error: err.message
            })
        }
        else {
            if (!user) {
                res.json({
                    error: "User not found"
                })
            }
            else {

                UserProfileObject.update({ username: user.username }, { avatar: config.get('host')+"/uploads/avatar/big/"+req.headers.filename}, function (error_, numberAffected) {
                    if (error_)
                        res.json({
                            error: error_
                        });
                    else
                    {
                        //console.log("Upload successfully");
                        res.json({
                            message: config.get('host')+"/uploads/avatar/big/"+req.headers.filename
                        })
                    }

                });
            }
        }
    });
});
app.post('/api/v1/picture', multer({
    storage: storage_pictures
}).single('upload'), function(req, res) {

    var imageobject = new ImageObject({
        content: host+'/uploads/pictures/'+req.headers.filename,
        index: parseInt(req.headers.index),
        description: req.headers.description
    });
    imageobject.save(function (err, imageobject) {
        if (err) {
            res.json({
                error: 'error: '+err.message
            })
        }
        else
        {
            if(!imageobject)
            {
                res.json({
                    error: 'save picture object error: '+err.message
                })
            }
            else
            {
                //console.log('filename : '+req.headers.filename);
                //console.log('index: '+req.headers.index);
                //console.log('description: '+req.headers.description);
                console.log(imageobject);
                res.json({
                    message: 'success',
                    id: imageobject._id,
                    content: imageobject.content
                })
            }
        }
    });

});

function createUserMileStoneIMG(image_object_id, location, user_id, callback){
    var milestoneobject = new MileStoneObject();
    milestoneobject.user_id = user_id;
    var bodyobject = new BodyObject();
    var images = [];
    images.push(image_object_id);
    bodyobject.images = images;
    utils.createTimeObject(function(err, time_object){
        if (err)
            res.json({
                error: err.message
            })
        else {
            utils.createLocationObject(location.lat, location.long, function(err, location_object){
                if (err)
                    res.json({
                        error: err.message
                    })
                else {
                    bodyobject.time = time_object._id;
                    bodyobject.location = location_object._id;
                    bodyobject.save(function (err, body_object) {
                        if (err)
                        {
                            callback(err);
                        }
                        else {
                            milestoneobject.content = body_object._id
                            milestoneobject.save(function (err, milestone_object) {
                                if (err)
                                {
                                    //console.log('error2');
                                    callback(err);
                                }
                                else {
                                    callback(null, milestone_object._id);
                                }
                            });
                        }
                    });
                }
            });
        }
    });


};
function createUserMileStoneVIDEO(video_object_id, location, user_id, callback){
    var milestoneobject = new MileStoneObject();
    milestoneobject.user_id = user_id;
    var bodyobject = new BodyObject();
    var videos = [];
    videos.push(video_object_id);
    bodyobject.videos = videos;
    utils.createTimeObject(function(err, time_object){
        if (err)
            res.json({
                error: err.message
            })
        else {
            utils.createLocationObject(location.lat, location.long, function(err, location_object){
                if (err)
                    res.json({
                        error: err.message
                    })
                else {
                    bodyobject.time = time_object._id;
                    bodyobject.location = location_object._id;
                    bodyobject.save(function (err, body_object) {
                        if (err)
                        {
                            callback(err);
                        }
                        else {
                            milestoneobject.content = body_object._id
                            milestoneobject.save(function (err, milestone_object) {
                                if (err)
                                {
                                    //console.log('error2');
                                    callback(err);
                                }
                                else {
                                    callback(null, milestone_object._id);
                                }
                            });
                        }
                    });
                }
            });
        }
    });


};
function isContentAtLocation(body_object_id, location, _id, milestones_id, image_object_id, callback){
    //console.log('check body object-->'+body_object_id+'--?--->'+_id);
    BodyObject.findOne({_id: body_object_id}, function(err, body_object){
        if (err)
            callback(null, null);
        else {
            var coords = [];
            coords[0] = location.long;
            coords[1] = location.lat;
            //console.log('search coords --->'+coords);
            LocationObject.find({
                coordinates:
                { $near: coords,
                    $maxDistance: 0.0001
                },_id: { $ne: _id } },function(err,ret){
                if (err)
                {
                    console.log(err.message);
                    callback(null, null, null, null);
                }

                else if (ret.length > 0)
                {
                    //console.log(ret);
                    var ids = [];
                    for (var i = 0; i < ret.length; i++)
                    {
                        ids.push(ret[i]._id);
                    }
                    var id = body_object.location;
                    //console.log(id);
                    var result = _.find(ids, function (o) { return o == id; });
                    if (result)
                    {
                        //console.log('body object-->'+body_object_id+' nam trong --->'+_id);
                        callback(null, body_object._id, milestones_id, image_object_id);
                    }
                    else
                    {
                        //console.log('body object-->'+body_object_id+' khong nam trong --->'+_id);
                        callback(null, null, null, null);
                    }
                }
                else
                {
                    //console.log('body object-->'+body_object_id+' khong nam trong --->'+_id);
                    callback(null, null, null, null);
                }

            })
        }
    });
}
function pushIMGtoBodyObject(body_object_id, _id, callback){
    BodyObject.update({_id:body_object_id},
        {$push: {
            "images":_id
        }
        },function (err, number) {
            if (err)
            {

                callback(err);
            }

            else
                callback(null, body_object_id);
        });

}
function pushVIDEOtoBodyObject(body_object_id, _id, callback){
    BodyObject.update({_id:body_object_id},
        {$push: {
            "videos":_id
        }
        },function (err, number) {
            if (err)
            {

                callback(err);
            }

            else
                callback(null, body_object_id);
        });

}
function seachMileStoneIMGWhoAtLocation(mileStones, location, _id, image_object_id, callback){
    var milestone_result;
    var length = mileStones.length;
    var hasTrue = false;

    asyncLoop(mileStones, function (item, next)
    {
        //console.log(item.content+'--->');
        if (!hasTrue)
        {
            isContentAtLocation(item.content, location,_id, item._id, image_object_id, function(err, body_object_id, milestones_id, image_object_id){
                //console.log(item.content+'vua tro ve'+milestones_id);
                if(err)
                {
                    next(err);
                    return;
                }

                else if (milestones_id != null && hasTrue == false)
                {
                    hasTrue = true;
                    milestone_result = milestones_id;
                    //console.log('hasTrue--->'+hasTrue);

                    pushIMGtoBodyObject(body_object_id, image_object_id, function (err, body_object_id){
                        if(err)
                        {
                            next(err);
                            return;
                        }
                        else
                            next();

                    });
                }
                else
                    next();
            });
        }
        else
            next();


    }, function (err)
    {
        if (err)
        {
            callback(null, null)
        }
        if (hasTrue)
        {
            //console.log('da insert vao '+milestone_result);
            callback(null, milestone_result)
        }
        else
        {
            callback(null, null)
        }
    });


}
function seachMileStoneVIDEOWhoAtLocation(mileStones, location, _id, video_object_id, callback){
    var milestone_result;
    var length = mileStones.length;
    var hasTrue = false;

    asyncLoop(mileStones, function (item, next)
    {
        //console.log(item.content+'--->');
        if (!hasTrue)
        {
            isContentAtLocation(item.content, location,_id, item._id, video_object_id, function(err, body_object_id, milestones_id, video_object_id){
                //console.log(item.content+'vua tro ve'+milestones_id);
                if(err)
                {
                    next(err);
                    return;
                }
                else if (milestones_id != null && hasTrue == false)
                {
                    hasTrue = true;
                    milestone_result = milestones_id;
                    //console.log('hasTrue--->'+hasTrue);

                    pushVIDEOtoBodyObject(body_object_id, video_object_id, function (err, body_object_id){
                        if(err)
                        {
                            next(err);
                            return;
                        }
                        else
                            next();
                    });
                }
                else
                    next();
            });
        }
        else
            next();
    }, function (err)
    {
        if (err)
        {
            callback(null, null)
        }
        if (hasTrue)
        {
            //console.log('da insert vao '+milestone_result);
            callback(null, milestone_result)
        }
        else
        {
            callback(null, null)
        }
    });
}
function insertImageToMileStone(image_object_id, location, user_id, _id, callback){
    MileStoneObject.find({user_id: user_id}, function (err, mileStones){
        if (err)
        {
            callback(err);
        }
        else {
            if (mileStones.length == 0) {
                //console.log(user_id+'-->new here');
                createUserMileStoneIMG(image_object_id, location, user_id, function (err, milestone_object_id){
                    if (err)
                    {
                        callback(err);
                    }
                    else {
                        callback(null, milestone_object_id);
                    }
                });
            }
            else
            {

                seachMileStoneIMGWhoAtLocation(mileStones, location, _id, image_object_id, function (err, milestone_object_id){
                    if (err)
                    {
                        callback(err);
                    }
                    else {
                        if (milestone_object_id)
                        {
                            //console.log(milestone_object_id);
                            callback(null, milestone_object_id);
                        }
                        else
                        {
                            createUserMileStoneIMG(image_object_id, location, user_id, function (err, milestone_object_id){
                                if (err)
                                {
                                    callback(err);
                                }
                                else {
                                    callback(null, milestone_object_id);
                                }
                            });
                        }
                    }
                });
            }
        }
    });
}
function insertVideoToMileStone(video_object_id, location, user_id, _id, callback){
    MileStoneObject.find({user_id: user_id}, function (err, mileStones){
        if (err)
        {
            callback(err);
        }
        else {
            if (mileStones.length == 0) {
                //console.log(user_id+'-->new here');
                createUserMileStoneVIDEO(video_object_id, location, user_id, function (err, milestone_object_id){
                    if (err)
                    {
                        callback(err);
                    }
                    else {
                        callback(null, milestone_object_id);
                    }
                });
            }
            else
            {

                seachMileStoneVIDEOWhoAtLocation(mileStones, location, _id, video_object_id, function (err, milestone_object_id){
                    if (err)
                    {
                        callback(err);
                    }
                    else {
                        if (milestone_object_id)
                        {
                            //console.log(milestone_object_id);
                            callback(null, milestone_object_id);
                        }
                        else
                        {
                            createUserMileStoneVIDEO(video_object_id, location, user_id, function (err, milestone_object_id){
                                if (err)
                                {
                                    callback(err);
                                }
                                else {
                                    callback(null, milestone_object_id);
                                }
                            });
                        }
                    }
                });
            }
        }
    });
}
app.post('/api/v1/milestone_img', passport.authenticate('bearer', { session: false }), function(req, res) {
    if (!req.body.milestone.hasOwnProperty("location"))
        res.json({
            error: 'bad data'
        })
    else {
        utils.createTimeObject(function(err, time_object){
            if (err)
                res.json({
                    error: err.message
                })
            else {
                utils.createLocationObject(req.body.milestone.location.lat, req.body.milestone.location.long, function(err, location_object){
                    if (err)
                        res.json({
                            error: err.message
                        })
                    else {
                        var imageobject = new ImageObject({
                            content: host+'/uploads/pictures/'+req.body.milestone.image.filename,
                            index: parseInt(req.body.milestone.image.index),
                            description: req.body.milestone.image.description,
                            time: time_object._id,
                            location: location_object._id
                        });
                        imageobject.save(function (err, image_object) {
                            if (err) {
                                res.json({
                                    error: err.message
                                })
                            }
                            else
                            {
                                insertImageToMileStone(image_object._id, req.body.milestone.location, req.user.userId, location_object._id, function(err, milestone_object_id){
                                    if (err)
                                    {

                                        res.json({
                                            error: err.message
                                        })
                                    }
                                    else {
                                        res.json({
                                            milestone_object_id: milestone_object_id
                                        })
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});
app.post('/api/v1/milestone_video', passport.authenticate('bearer', { session: false }), function(req, res) {
    if (!req.body.milestone.hasOwnProperty("location"))
        res.json({
            error: 'bad data'
        })
    else {
        utils.createTimeObject(function(err, time_object){
            if (err)
                res.json({
                    error: err.message
                })
            else {
                utils.createLocationObject(req.body.milestone.location.lat, req.body.milestone.location.long, function(err, location_object){
                    if (err)
                        res.json({
                            error: err.message
                        })
                    else {
                        var videoobject = new VideoObject({
                            content: host+'/uploads/videos/'+req.body.milestone.video.filename,
                            index: parseInt(req.body.milestone.video.index),
                            description: req.body.milestone.video.description,
                            time: time_object._id,
                            location: location_object._id
                        });
                        videoobject.save(function (err, videoobject) {
                            if (err) {
                                res.json({
                                    error: err.message
                                })
                            }
                            else
                            {
                                insertVideoToMileStone(videoobject._id, req.body.milestone.location, req.user.userId, location_object._id, function(err, milestone_object_id){
                                    if (err)
                                    {

                                        res.json({
                                            error: err.message
                                        })
                                    }
                                    else {
                                        res.json({
                                            milestone_object_id: milestone_object_id
                                        })
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});
app.post('/api/v1/video', multer({
    storage: storage_videos
}).single('upload'), function(req, res) {
    var videoobject = new VideoObject({
        content: host+'/uploads/videos/'+req.headers.filename,
        index: parseInt(req.headers.index),
        description: req.headers.description
    });
    videoobject.save(function (err, videoobject) {
        if (err) {
            res.json({
                message: 'error: '+err.message
            })
        }
        else
        {
            if(!videoobject)
            {
                res.json({
                    message: 'save video object error: '+err.message
                })
            }
            else
            {
                //console.log('filename : '+req.headers.filename);
                //console.log('index: '+req.headers.index);
                //console.log('description: '+req.headers.description);
                //console.log('OK');
                res.json({
                    message: 'success',
                    id: videoobject._id,
                    content: videoobject.content
                })
            }
        }
    });


});

app.get('/api/v1/status/comment', function (req, res) {
    //console.log("_id :" + req.headers._id);
    //console.log("start : " + req.headers.start);
    //console.log("limit : " + req.headers.limit);
    var check = true;
    if (!req.headers.start)
        check = false;
    else if (!req.headers.limit)
        check = false;
    if (check==false)
        res.json({
            message: 'bad data'
        });

    else
    {
        StatusPackage.findOne({_id:req.headers._id},function(err, status_package){
            if (err)
                res.json(
                    {
                        message :err.message
                    });
            else if (status_package)
            {
                //console.log(status_package);
                InteractionObject.findOne({_id:status_package.interaction},function(err, interaction_object){
                    if (err)
                        res.json(
                            {
                                message :err.message
                            });
                    else if (!interaction_object)
                    {
                        res.json(
                            {
                                message :'interaction object not found'
                            });
                    }
                    else
                    {
                        getAllComment(interaction_object.comments,parseInt(req.headers.start),parseInt(req.headers.limit), function(err, comments)
                        {
                            if (err)
                                res.json(
                                    {
                                        message :'interaction object not found'
                                    });
                            else
                            {
                                res.json(
                                    {
                                        toltal_comments:interaction_object.comments.length,
                                        result_comments: comments.length,
                                        comments :comments
                                    });
                            }
                        });

                    }
                });
            }
            else
                res.json(
                    {
                        message :'status not found'
                    });
        });
    }
});
app.get('/api/v1/status/like', function (req, res) {

    //console.log("_id :" + req.headers._id);
    //console.log("start : " + req.headers.start);
    //console.log("limit : " + req.headers.limit);
    var check = true;
    if (!req.headers._id)
        check = false;
    if (!req.headers.start)
        check = false;
    if (!req.headers.limit)
        check = false;
    if (check==false)
        res.json({
            message: 'bad data'
        });

    else
    {

        StatusPackage.findOne({_id:req.headers._id},function(err, status_package){
            if (err)
                res.json(
                    {
                        message :err.message
                    });
            else if (status_package)
            {
                //console.log(status_package._id);
                OwnerObject.find({target_id:status_package._id, target_class:'like_status_package'},{user_id:1},function(err, owner_objects){
                    if (err)
                        res.json(
                            {
                                err :err.message
                            });
                    else if (!owner_objects)
                    {
                        res.json(
                            {
                                err :'interaction object not found'
                            });
                    }
                    else
                    {
                        //console.log(owner_objects);
                        getAllUserLike(owner_objects,parseInt(req.headers.start),parseInt(req.headers.limit), function(err, likes)
                        {
                            if (err)
                                res.json(
                                    {
                                        message :err.message
                                    });
                            else
                            {
                                res.json(
                                    {
                                        toltal_likes:owner_objects.length,
                                        result_likes: likes.length,
                                        likes :likes
                                    });
                            }
                        });

                    }
                });
            }
            else
                res.json(
                    {
                        message :'status not found'
                    });
        });
    }
});
app.get('/api/v1/status/here', function(req, res){
    //console.log(parseFloat(req.headers.lat));
    //console.log(parseFloat(req.headers.long));
    LocationObject.find({lat:parseFloat(req.headers.lat),long:parseFloat(req.headers.long)}, function(err, location_object){
        if(err){
            //console.log('error');
            res.json({
                err: err.message
            });
        }
        else {
            if (location_object.length==0){
                //console.log('not found');
                res.json({
                    err: 'not found'
                });
            }
            else {

                var _ids = [];
                var length = location_object.length;

                for (var j = 0; j < length; j++)
                    _ids.push(location_object[j]._id);
                StatusPackage.find( { location : { $in : _ids } },{ _id : 1 }, function(err, result){
                    if (result)
                    {
                        console.log(result.length + ' package(s) found');
                        if (result.length > 0)
                            getAllStatusPackage(result, function (err, status_packages){
                                if (err)
                                    res.json({
                                        message : err.message
                                    });
                                else
                                    res.json({
                                        status_packages : status_packages
                                    });
                            });
                        else
                            res.json({
                                status_packages : []
                            });
                    }
                });

            }
        }
    });
});
app.get('/api/v1/status/id', function(req, res){
    StatusPackage.findOne( { _id : req.headers._id},{ _id : 1 }, function(err, status_package){
        if (err)
            res.json({
                error: err.message
            });
        else if (!status_package)
            res.json({
                error: 'no package found'
            });
        else {
            getStatusPackageByID(req.headers._id,function (err, statuspackage){
                if (err)
                    res.json({
                        error: err.message
                    });
                else if (statuspackage){
                    res.json({
                        status_package: statuspackage
                    });
                }
                else {
                    res.json({
                        error: 'no package found'
                    });
                }
            });
        }
    });
});
app.get('/api/v1/video/id', function (req, res) {
    VideoObject.findOne({_id: req.headers._id}, function (err, video) {
        if (err)
            res.json({
                err: err.message
            });
        else{
            if (video)
                res.json({
                    url: video.content
                });
            else
                res.json({
                    err: 'no video found'
                });
        }
    });
});
app.get('/api/v1/picture/id', function (req, res) {
    ImageObject.findOne({_id: req.headers._id}, function (err, image) {
        if (err)
            res.json({
                err: err.message
            });
        else{
            if (image)
                res.json({
                    url: image.content
                });
            else
                res.json({
                    err: 'no video found'
                });
        }
    });
});

app.get('/api/v1/status', function (req, res) {
    LocationObject.find({ coordinates :
    { $geoWithin :{ $box : [ [ req.headers.left , req.headers.bottom ] , [ req.headers.right , req.headers.top ] ]} }
    },function(err,locationlbject){
        if(err){
            //console.log('error');
            res.json({
                err: err.message
            });
        }
        else {
            if (locationlbject.length==0){
                //console.log('not found');
                res.json({
                    message: 'not found'
                });
            }
            else {

                var _ids = [];
                var length = locationlbject.length;

                for (var j = 0; j < length; j++)
                    _ids.push(locationlbject[j]._id);
                StatusPackage.find( { location : { $in : _ids } },{ _id : 1 }, function(err, result){
                    if (result)
                    {
                        console.log(result.length + ' package(s) found');
                        if (result.length > 0)
                            getAllStatusPackage(result, function (err, status_packages){
                                if (err)
                                    res.json({
                                        message : err.message
                                    });
                                else
                                    res.json({
                                        status_packages : status_packages
                                    });
                            });
                        else
                            res.json({
                                status_packages : []
                            });
                    }
                });

            }
        }
    });

});
app.post('/api/v1/status/comment', passport.authenticate('bearer', { session: false }), function(req, res){
    //console.log(req.body.data);
    //console.log(req.body.target);
    var check = true;
    if (!req.body.hasOwnProperty("data")||!req.body.hasOwnProperty("target"))
        check = false;
    else
    {
        if (!req.body.data.hasOwnProperty("content"))
            check = false;
        else if (!req.body.data.hasOwnProperty("content"))
            check = false;
        else if (!req.body.data.content.hasOwnProperty("text_object"))
            check = false;
        else if (!req.body.data.content.text_object.hasOwnProperty("content"))
            check = false;
        else if (!req.body.data.content.text_object.hasOwnProperty("index"))
            check = false;
        if (!req.body.data.hasOwnProperty("class"))
            check = false;
        else if (!req.body.target.hasOwnProperty("id"))
            check = false;
        else if (!req.body.target.hasOwnProperty("class"))
            check = false;
    }
    if (!check)
        res.json({
            err: 'error: bad data'
        })
    else
    {
        //text object
        var content = req.body.data.content.text_object.content;
        var index = req.body.data.content.text_object.index;

        utils.createTextObject(null,content,index, function (err, text_object) {
            //console.log('callback here');
            if (err) {
                res.json({
                    err: 'Create text object error'
                });
            }
            else if(text_object){
                var bodyobject = new BodyObject({
                    text: text_object._id
                });
                if (req.body.data.content.videos){
                    bodyobject.videos = req.body.data.content.videos;
                }
                if (req.body.data.content.images){
                    bodyobject.images = req.body.data.content.images;
                }
                bodyobject.location_object = null;
                if (req.body.data.content.location_object){
                    bodyobject.location_object = req.body.data.content.location_object;
                }
                utils.createBodyObject(bodyobject, function (err, bodyobject) {
                    if (err) {
                        res.json({
                            message: 'Create body object error'
                        });
                    }
                    else {
                        var comment_content_object = new CommentContentObject({
                            content: bodyobject._id
                        });


                        createCommentContentObject(comment_content_object, function (err, comment_content_object) {
                            if (err) {
                                res.json({
                                    message: 'Create Comment Content Object error'
                                });
                            }
                            else {
                                var comment_object = new CommentObject({
                                    user_profile: req.user.userId,
                                    content: comment_content_object._id
                                });

                                var location = null;
                                if (req.body.data.location_object)
                                {
                                    location =
                                    {lat: req.body.data.location_object.lat,
                                        long: req.body.data.location_object.long
                                    };
                                }


                                createCommentObject(location, comment_object, function (err, comment_object) {
                                    if (err) {
                                        res.json({
                                            message: 'Create Comment Object error'
                                        });
                                    }
                                    else {

                                        insertCommentObjectToStatusPackage(comment_object._id, req.body.target.id, function (err, number){
                                            if (err) {
                                                res.json({
                                                    message: 'Insert Comment Object error'
                                                });
                                            }
                                            else {
                                                res.json({
                                                    comment_id: comment_object._id
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});
app.post('/api/v1/status/like', passport.authenticate('bearer', { session: false }), function(req, res){
    //console.log(req.body.data);
    //console.log(req.body.target);
    var check = true;
    if (!req.body.hasOwnProperty("data")||!req.body.hasOwnProperty("target"))
        check = false;
    else
    {
        if (!req.body.data.hasOwnProperty("location_object"))
            check = false;
        else if (!req.body.data.location_object.hasOwnProperty("lat"))
            check = false;
        else if (!req.body.data.location_object.hasOwnProperty("long"))
            check = false;
        else if (!req.body.target.hasOwnProperty("id"))
            check = false;
        else if (!req.body.target.hasOwnProperty("class"))
            check = false;
    }
    if (!check)
        res.json({
            message: 'error: bad data'
        })
    else
    {

        isLiked_status_package(req.user.userId,req.body.target.id,function (err, result) {
            if (err)
                res.json({
                    err: err.message
                })
            if (result =='n' )
            {

                utils.createLocationObject(req.body.data.location_object.lat, req.body.data.location_object.long, function (err, locationobject) {
                    if (err)
                        res.json({
                            error: err.message
                        });
                    else {
                        utils.createTimeObject(function (err, timeobject) {
                            if (err)
                                callback(err);
                            else {
                                var likeobject = new LikeObject({
                                    user_profile: req.body.data.user_profile,
                                    location: locationobject._id,
                                    time: timeobject._id
                                });

                                likeobject.save(function (err, likeobject) {
                                    if (err)
                                        res.json({
                                            error: err.message
                                        });
                                    else {
                                        insertLikeObjectToStatusPackage(likeobject._id, req.body.target.id, function (err, number){
                                            if (err) {
                                                res.json({
                                                    message: 'Insert Like Object error'
                                                });
                                            }
                                            else {

                                                var ownerobject = new OwnerObject({
                                                    user_id: req.user.userId,
                                                    target_id: req.body.target.id,
                                                    target_class: 'like_status_package',
                                                    time: Date.now()
                                                });
                                                ownerobject.save(function (err, ownerobject) {
                                                    if (err)
                                                        callback(err);
                                                    else {
                                                        res.json({
                                                            likeobject_id: likeobject._id
                                                        });
                                                    }
                                                });

                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
            else
            {
                res.json({
                    error: 'this user has like this status'
                });
            }
        });

    }
});
app.post('/api/v1/requestfriend', passport.authenticate('bearer', { session: false }), function (req, res) {
    User.findOne({ _id: req.user.userId }, function (err, user) {
        if (err) {
            res.json({
                error: err.message
            })
        }
        else
        {
            if (!user) {
                res.json({
                    message: "user not found"
                })
            }
            else {
                User.findOne({ username: req.headers.username }, function (err, user1) {
                    if (err) {
                        res.json({
                            error: err.message
                        })
                    }
                    else
                    {
                        if (!user1) {
                            res.json({
                                error: "user not found"
                            })
                        }
                        else {
                            User.requestFriend(user._id, user1._id, function(err_,fships)
                            {
                                if (err_)
                                {
                                    console.log(err_.message);
                                    res.json({
                                        error: err_.message
                                    });
                                }

                                res.json({
                                    message: fships.friend.status
                                });
                            });
                        }
                    }
                });
            }
        }
    });
});
app.post('/api/v1/updateprofile', passport.authenticate('bearer', { session: false }), function (req, res) {
    User.findOne({ _id: req.user.userId }, function (err, user) {
        if (err) {
            res.json({
                error: err.message
            })
        }
        else
        {
            if (!user) {
                res.json({
                    error: "user not found"
                })
            }
            else {
                UserProfileObject.update({ username: user.username }, { display_name: req.headers.display_name}, function (error_, numberAffected) {
                    if (error_)
                        res.json({
                            error: error_
                        });
                    else {
                        res.json({
                            message: "OK"
                        });
                    }
                });

            }
        }
    });
});
app.post('/api/v1/requestacceptfriend', passport.authenticate('bearer', { session: false }), function (req, res) {
    User.findOne({ _id: req.user.userId }, function (err, user) {
        if (err) {
            res.json({
                error: err.message
            })
        }
        else
        {
            if (!user) {
                res.json({
                    message: "user not found"
                })
            }
            else {
                User.findOne({ username: req.headers.username }, function (err, user1) {
                    if (err) {
                        res.json({
                            error: err.message
                        })
                    }
                    else
                    {
                        if (!user1) {
                            res.json({
                                error: "user not found"
                            })
                        }
                        else {
                            User.requestFriend(user._id, user1._id, function(err_,fships)
                            {
                                if (err_)
                                {
                                    console.log(err_.message);
                                    res.json({
                                        error: err_.message
                                    });
                                }
                                else {
                                    User.requestFriend(user1._id, user._id, function(err_,fships){

                                    });
                                    res.json({
                                        message: fships.friend.status
                                    });
                                }

                            });
                        }
                    }
                });
            }
        }
    });
});
app.get('/api/v1/validate_user', passport.authenticate('bearer', { session: false }), function (req, res) {
    var json_object = new UserObjectResponse();

    User.findOne({ _id: req.user.userId }, function (err, user) {
        if (err) {
            res.json({
                error: err.message
            })
        }
        else
        {
            User.update({ _id: user._id }, { lastlogindate: Date.now(), state : true}, function (err, numberAffected) {
                if (err)
                    console.log("User login update error");
            });
            json_object._id = user._id;
            UserProfileObject.findOne({ username: user.username}, function(err, user) {
                if(err)
                {
                    res.json({
                        error: err.message
                    })
                }
                else
                {
                    json_object.username = user.username;
                    json_object.email = user.email;
                    json_object.displayname = user.display_name;
                    res.json({
                        user_info: json_object
                    })
                }
            });
        }
    });
});
app.post('/api/v1/removefriend', passport.authenticate('bearer', { session: false }), function (req, res) {
    User.findOne({ _id: req.user.userId }, function (err, user) {
        if (err) {
            res.json({
                error: err.message
            })
        }
        else
        {
            if (!user) {
                res.json({
                    error: "user not found"
                })
            }
            else {
                User.findOne({ username: req.headers.username }, function (err, user1) {
                    if (err) {
                        res.json({
                            error: err.message
                        })
                    }
                    else
                    {
                        if (!user1) {
                            res.json({
                                message: "user not found"
                            })
                        }
                        else {
                            user.removeFriend(user1, function(err_,cb)
                            {
                                if (err_) {
                                    res.json({
                                        error: err_.message
                                    })
                                }
                                else
                                {
                                    res.json({
                                        message: "OK"
                                    });

                                }
                            });
                        }
                    }
                });

            }
        }
    });
});

app.post('/api/v1/status', passport.authenticate('bearer', { session: false }), function(req,res){
    User.findOne({ _id: req.user.userId }, function (err, user) {
        if (err) {
            res.json({
                message: err.message,
                errmsg: err.errmsg
            })
        }
        else {
            if (!user) {
                res.json({
                    message: "User not found"
                })
            }
            else {
                var check = true;
                if (!req.body.hasOwnProperty("status_package"))
                    check = false;
                else
                {
                    if (!req.body.status_package.hasOwnProperty("status_object"))
                        check = false;
                    else {
                        if (!req.body.status_package.status_object.hasOwnProperty("body_object"))
                            check = false;
                        else {
                            if (!req.body.status_package.status_object.body_object.hasOwnProperty("text_object"))
                                check = false;
                            else
                            {
                                if (!req.body.status_package.status_object.body_object.text_object.hasOwnProperty("content"))
                                    check = false;
                                if (!req.body.status_package.status_object.body_object.text_object.hasOwnProperty("index"))
                                    check = false;
                            }
                        }
                    }
                }
                if (!check)
                    res.json({
                        message: 'error: bad data'
                    })
                else
                {
                    //text object
                    var location_object = null;
                    var content = req.body.status_package.status_object.body_object.text_object.content;
                    var index = req.body.status_package.status_object.body_object.text_object.index;
                    if (req.body.status_package.status_object.body_object.text_object.location_object)
                    {
                        location_object = req.body.status_package.status_object.body_object.text_object.location_object;
                    }

                    utils.createTextObject(location_object,content,index, function (err, text_object) {
                        //console.log('callback here');
                        if (err) {
                            res.json({
                                message: 'Create text object error'
                            });
                        }
                        else {

                            var bodyobject = new BodyObject({
                                text: text_object._id
                            });
                            if (req.body.status_package.status_object.body_object.videos){
                                bodyobject.videos = req.body.status_package.status_object.body_object.videos;
                            }
                            if (req.body.status_package.status_object.body_object.images){
                                bodyobject.images = req.body.status_package.status_object.body_object.images;
                            }
                            bodyobject.location_object = null;
                            if (req.body.status_package.status_object.body_object.location_object){
                                bodyobject.location_object = req.body.status_package.status_object.body_object.location_object;
                            }
                            utils.createBodyObject(bodyobject, function (err, bodyobject) {
                                if (err) {
                                    res.json({
                                        message: 'Create body object error'
                                    });
                                }
                                else {
                                    var statusobject = new StatusObject({
                                        content: bodyobject._id
                                    });
                                    if (!req.body.status_package.status_object.embed){
                                        statusobject.embed = req.body.status_package.status_object.embed;
                                    }
                                    if (!req.body.status_package.status_object.categories){
                                        statusobject.categories = req.body.status_package.status_object.categories;
                                    }
                                    statusobject.location_object = null;
                                    if (req.body.status_package.status_object.location_object){
                                        statusobject.location_object = req.body.status_package.status_object.location_object;
                                    }
                                    createStatusObject(statusobject, function (err, statusobject) {
                                        if (err) {
                                            res.json({
                                                message: 'Create status object error'
                                            });
                                        }
                                        else {
                                            createInteractionObject(function (err, interaction_object) {
                                                if (err) {
                                                    res.json({
                                                        message: 'Create Interaction Object error'
                                                    });
                                                }
                                                else {

                                                    if (!req.body.status_package.hasOwnProperty("privacy_object"))
                                                    {
                                                        var statuspackage = new StatusPackage({
                                                            content: statusobject._id,
                                                            interaction: interaction_object._id
                                                        });
                                                        statuspackage.location_object = null;
                                                        if (req.body.status_package.location_object) {
                                                            statuspackage.location_object = req.body.status_package.location_object;
                                                        }
                                                        statuspackage.user_profile = user._id;
                                                        //console.log(statuspackage);
                                                        createStatusPackage(statuspackage, function (err, statuspackage) {
                                                            if (err) {
                                                                res.json({
                                                                    message: 'Create status package error'
                                                                });
                                                            }
                                                            else {
                                                                var ownerobject = new OwnerObject({
                                                                    user_id: req.user.userId,
                                                                    target_id: statuspackage._id,
                                                                    target_class: 'post_status_package',
                                                                    time: Date.now()
                                                                });
                                                                //console.log(ownerobject);
                                                                ownerobject.save(function (err, ownerobject) {
                                                                    if (err)
                                                                        callback(err);
                                                                    else {
                                                                        res.json({
                                                                            status_package_id: statuspackage._id
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                    else
                                                    {
                                                        var privacyobject = new PrivacyObject();
                                                        privacyobject.stricts = req.body.status_package.privacy_object.stricts;
                                                        if (req.body.status_package.privacy_object.whitelist)
                                                            privacyobject.whitelist = req.body.status_package.privacy_object.whitelist;
                                                        if (req.body.status_package.privacy_object.blacklist)
                                                            privacyobject.blacklist = req.body.status_package.privacy_object.blacklist;
                                                        if (req.body.status_package.privacy_object.enable != undefined)
                                                            privacyobject.enable = req.body.status_package.privacy_object.enable;
                                                        privacyobject.location_object = null;
                                                        if (req.body.status_package.privacy_object.location_object){
                                                            privacyobject.location_object = req.body.status_package.privacy_object.location_object;
                                                        }
                                                        createPrivacyObject(privacyobject, function (err, privacyobject) {
                                                            if (err) {
                                                                res.json({
                                                                    message: 'Create privacy object error'
                                                                });
                                                            }
                                                            else {
                                                                var statuspackage = new StatusPackage({
                                                                    privacy: privacyobject._id,
                                                                    content: statusobject._id,
                                                                    interaction: interaction_object._id
                                                                });

                                                                statuspackage.location_object = null;
                                                                if (req.body.status_package.location_object) {
                                                                    statuspackage.location_object = req.body.status_package.location_object;
                                                                }
                                                                statuspackage.user_profile = user._id;
                                                                //console.log(statuspackage);
                                                                createStatusPackage(statuspackage, function (err, statuspackage) {
                                                                    if (err) {
                                                                        res.json({
                                                                            message: 'Create status package error'
                                                                        });
                                                                    }
                                                                    else {
                                                                        var ownerobject = new OwnerObject({
                                                                            user_id: req.user.userId,
                                                                            target_id: statuspackage._id,
                                                                            target_class: 'post_status_package',
                                                                            time: Date.now()
                                                                        });
                                                                        //console.log(ownerobject);
                                                                        ownerobject.save(function (err, ownerobject) {
                                                                            if (err)
                                                                                callback(err);
                                                                            else {
                                                                                res.json({
                                                                                    status_package_id: statuspackage._id
                                                                                });
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            }
        }
    });
});

app.get('/api/v1/status/myfriends',passport.authenticate('bearer', { session: false }), function(req, res){
    User.findOne({ _id: req.user.userId }, function (err, user) {
        if (err) {

            res.json({
                error: err.message
            })
        }
        else
        {

            if (!user) {
                res.json({
                    error: "user not found"
                })
            }
            else {

                User.getFriends(user, function (err_, friends) {
                    if (err_) {
                        res.json({
                            error: err_.message
                        })
                    }
                    else if (friends.length > 0)
                    {
                        //console.log(friends.length);
                        var friend_ids = [];
                        for (var i=0;i<friends.length;i++)
                        {
                            friend_ids.push(friends[i]._id);
                        }
                        //console.log(friend_ids);
                        getAllStatusPackageOfMyFriends(friend_ids,parseInt(req.headers.index), parseInt(req.headers.limit), function (err, status_packages){
                            if (err)
                                res.json({
                                    message : err.message
                                });
                            else if (status_packages.length > 0)
                            {
                                //console.log(status_packages_id);
                                res.json({
                                    status_packages : status_packages
                                });
                            }
                            else
                                res.json({
                                    message : 'no package found'
                                });
                        });
                    }
                    else
                        res.json({
                            message : 'no package found'
                        });
                });
            }
        }
    });
});
function getAllStatusPackageOfMyFriends(friend_ids, start, limit, callback)
{
    //console.log(friend_ids);
    getUserStatusPackageIDByID(friend_ids, start, limit, function (err, status_id_list){
        if (err)
            callback(err);
        else if (status_id_list.length > 0)
        {
            //console.log(status_id_list);
            getAllStatusPackage(status_id_list, function (err, status_packages){
                if (err)
                    callback(err);
                else
                    callback(null,status_packages);
            });

        }
        else
        {
            callback(null, []);
        }
    });
}
function getUserStatusPackageIDByID(user_ids, start, limit, callback){
    var starttime = new Date();
    starttime.setHours(0,0,0,0);
    var endtime = new Date();
    endtime.setHours(23,59,59,999);

    OwnerObject.find({user_id: { $in: user_ids }, target_class:'post_status_package',time: {$gte: starttime.getTime(), $lt: endtime.getTime()}},function(err, owner_object){
        if (err)
            callback(err);
        else if (owner_object.length > 0)
        {
            var list = [];

            //console.log(owner_object);

            var length = owner_object.length;
            var end = length;
            var comments_result = [];
            //console.log('length:'+length+';start'+start+';end:'+end);
            if (start>=0 && limit>0)
            {
                if (start>=length)
                    callback(null, []);
                else {
                    if (limit+start>length)
                        end = length;
                    else {
                        end = start + limit;
                        //console.log('length:'+length+';start'+start+';end:'+end);
                    }
                    for (var i = start; i < end; i++)
                        list.push(owner_object[i].target_id);
                    callback(null, list);
                }
            }
            else
            {
                var err = {
                    message:'bad range limit'
                }

                callback(err);
            }
        }
        else
        {
            callback(null, []);
        }
    });
}
app.get('/api/v1/get_user_id_by_username', function(req, res)
{
    User.findOne({ username: req.headers.username }, function (err, user) {
        if (err)
            res.json({
                error: err.message
            })
        else
        {
            if (user)
                res.json({
                    user_id: user._id
                })
            else
                res.json({
                    err: 'not found'
                })
        }

    });
});
app.get('/api/v1/searchallfriend_orginal', passport.authenticate('bearer', { session: false }), function (req, res) {

    User.findOne({ _id: req.user.userId }, function (err, user) {
        if (err) {
            res.json({
                error: err.message
            })
        }
        else
        {
            if (!user) {
                res.json({
                    error: "user not found"
                })
            }
            else {
                var except_list = [];
                except_list.push(user.username);

                if (req.headers.keyword == '*' || req.headers.keyword == '')
                    UserProfileObject.find({ "username" : { $nin: except_list } },{__v:0,category_packages:0}, function(err, user_profile_object) {
                        if(err)
                        {
                            res.json({
                                error: err.message
                            })
                        }
                        else
                        {

                            var users_list = [];
                            var length = user_profile_object.length;
                            if (length == 0)
                                res.json({
                                    users: []
                                })
                            else
                            {
                                for (var i =0; i< length; i++)
                                {

                                    var json_object ={
                                        username: user_profile_object[i].username,
                                        display_name: user_profile_object[i].display_name,
                                        avatar: user_profile_object[i].avatar,
                                        email: user_profile_object[i].email,
                                        friendship: ''
                                    };
                                    users_list.push(json_object);
                                }
                                res.json({
                                    users: users_list
                                });
                            }
                        }
                    });
                else
                {
                    //console.log(except_list);

                    UserProfileObject.find({ $and : [ { "username" : {$regex : ".*"+req.headers.keyword+".*"} }, { "username" : { $nin: except_list } } ] },{__v:0,category_packages:0}, function(err, users) {
                        if(err)
                        {
                            res.json({
                                error: err.message
                            })
                        }
                        else
                        {
                            var users_list = [];
                            for (var i =0; i< users.length; i++)
                            {
                                var json_object ={
                                    username: users[i].username,
                                    avatar: users[i].avar,
                                    email: users[i].email,
                                    friendship: ''
                                };
                                users_list.push(json_object);
                            }

                            res.json({
                                users: users_list
                            });
                        }
                    });
                }
            }
        }
    });
});
function getUserTokenByUserName(item, callback){
    User.findOne({ username: item.username }, function (err, user) {
        if (err) {
            callback(err);
        }
        else
        {
            if (!user) {
                var err = {
                    message:'user not found'
                }
                callback(err);
            }
            else {
                AccessToken.findOne({ userId: user._id }, function(err, token) {

                    if (err) {
                        callback(err);
                    }
                    else
                    {
                        if (token)
                        {
                            item.token = token.token;
                            callback(null, item);
                        }

                        else
                        {
                            var err = {
                                message:'user not found'
                            }
                            callback(err);
                        }
                    }

                });
            }
        }
    });
}
app.get('/api/v1/searchallfriend', passport.authenticate('bearer', { session: false }), function (req, res) {

    User.findOne({ _id: req.user.userId }, function (err, user) {
        if (err) {
            res.json({
                error: err.message
            })
        }
        else
        {
            if (!user) {
                res.json({
                    error: "user not found"
                })
            }
            else {
                var except_list = [];
                except_list.push(user.username);

                if (req.headers.keyword == '*' || req.headers.keyword == '')
                    UserProfileObject.find({ "username" : { $nin: except_list } },{__v:0,category_packages:0}, function(err, user_profile_object) {
                        if(err)
                        {
                            res.json({
                                error: err.message
                            })
                        }
                        else
                        {
                            var users_list0 = [];
                            var length = user_profile_object.length;
                            if (length == 0)
                                res.json({
                                    users: []
                                })
                            else
                            {
                                for (var i =0; i< length; i++)
                                {
                                    //console.log(user_profile_object[i]);
                                    var json_object ={
                                        username: user_profile_object[i].username,
                                        display_name: user_profile_object[i].display_name,
                                        avatar: user_profile_object[i].avatar,
                                        email: user_profile_object[i].email,
                                        friendship: ''
                                    };
                                    users_list0.push(json_object);
                                }
                                var users_list = [];
                                asyncLoop(users_list0, function (item, next)
                                {
                                    getUserTokenByUserName(item, function(err, user_profile_with_token){
                                        if(err)
                                        {
                                            next(err);
                                            return;
                                        }
                                        else
                                        {
                                            users_list.push(user_profile_with_token);
                                            next();
                                        }
                                    });
                                }, function (err)
                                {
                                    if (err)
                                    {
                                        res.json({
                                            error: err.message
                                        })
                                    }
                                    else
                                        res.json({
                                            users: users_list
                                        });
                                });

                            }
                        }
                    });
                else
                {
                    //console.log(except_list);

                    UserProfileObject.find({ $and : [ { "username" : {$regex : ".*"+req.headers.keyword+".*"} }, { "username" : { $nin: except_list } } ] },{__v:0,category_packages:0}, function(err, user_profile_object) {
                        if(err)
                        {
                            res.json({
                                error: err.message
                            })
                        }
                        else
                        {
                            var users_list0 = [];
                            var length = user_profile_object.length;
                            if (length == 0)
                                res.json({
                                    users: []
                                })
                            else
                            {
                                for (var i =0; i< length; i++)
                                {
                                    //console.log(user_profile_object[i]);
                                    var json_object ={
                                        username: user_profile_object[i].username,
                                        display_name: user_profile_object[i].display_name,
                                        avatar: user_profile_object[i].avatar,
                                        email: user_profile_object[i].email,
                                        friendship: ''
                                    };
                                    users_list0.push(json_object);
                                }
                                var users_list = [];
                                asyncLoop(users_list0, function (item, next)
                                {
                                    getUserTokenByUserName(item, function(err, user_profile_with_token){
                                        if(err)
                                        {
                                            next(err);
                                            return;
                                        }
                                        else
                                        {
                                            users_list.push(user_profile_with_token);
                                            next();
                                        }
                                    });
                                }, function (err)
                                {
                                    if (err)
                                    {
                                        res.json({
                                            error: err.message
                                        })
                                    }
                                    else
                                        res.json({
                                            users: users_list
                                        });
                                });

                            }
                        }
                    });
                }
            }
        }
    });
});
function getUserProfile(username, friendship, callback){
    //console.log(friendship);
    UserProfileObject.findOne({username :username}, function(err, profile){
        if(err)
        {
            callback(err);
        }
        else
        {
            var json_object = {
                username : profile.username,
                email : profile.email,
                displayname : profile.display_name,
                avatar : profile.avatar,
                friendship :  friendship
            }
            //console.log(json_object);
            callback(null, json_object);
        }
    });
}
function getUserProfileList(username_list, callback){
    var user_profile_list = [];
    var length = username_list.length;
    asyncLoop(username_list, function (item, next)
    {
        getUserProfile(item.username, item.status, function(err, profile){
            if(err)
            {
                next(err);
                return;
            }
            else
            {
                user_profile_list.push(profile);
                next();
            }
        });
    }, function (err)
    {
        if (err)
        {
            callback(err)
        }
        else
            callback(null, user_profile_list);
    });
}
app.get('/api/v1/distance',function(req, res){
    var result = distance(parseFloat(req.headers.lat1),parseFloat(req.headers.long1),parseFloat(req.headers.lat2),parseFloat(req.headers.long2));
    res.json({
        distance: result
    })
});
app.get('/api/v1/get_location_nearby',function(req, res){
    var coords = [];
    coords[0] = req.headers.long || 0;
    coords[1] = req.headers.lat || 0;
    console.log(coords);
    LocationObject.find({
        coordinates:
        { $near: coords,
            $maxDistance: req.headers.maxdistance
        }},function(err,ret){
        if (err)
            res.json({
                error: err.message
            })
        else
            res.json({
                locations: ret
            })
    })

});
app.get('/api/v1/get_location_bound',function(req, res){

    LocationObject.find({ coordinates :
    { $geoWithin :{ $box : [ [ req.headers.left , req.headers.bottom ] , [ req.headers.right , req.headers.top ] ]} }
    },function(err,ret){
        if (err)
            res.json({
                error: err.message
            })
        else
            res.json({
                locations: ret
            })
    });

});
app.get('/api/v1/getfriend', passport.authenticate('bearer', { session: false }), function (req, res) {
    if (!req.headers.limit)
    {
        User.findOne({ _id: req.user.userId }, function (err, user) {
            if (err) {
                res.json({
                    error: err.message
                })
            }
            else
            {
                if (!user) {
                    res.json({
                        error: "user not found"
                    })
                }
                else {

                    User.getFriends(user, function (err_, myfriends) {
                        if (err_) {
                            res.json({
                                error: err_.message
                            })
                        }
                        else
                        {
                            var username_list = [];
                            for (var i =0; i< myfriends.length; i++)
                            {
                                //console.log(myfriends[i].friend);
                                var json_object = {
                                    username : myfriends[i].friend.username,
                                    status : myfriends[i].status
                                }
                                username_list.push(json_object);
                            }
                            if (username_list.length > 0)
                            {

                                getUserProfileList(username_list, function(err, friend_list){
                                    if (err)
                                        res.json({
                                            error: err_.message
                                        })
                                    else {
                                        res.json({
                                            users: friend_list
                                        })
                                    }
                                });
                            }
                            else {
                                res.json({
                                    users: []
                                })
                            }

                        }

                    });
                }
            }
        });
    }
    else {
        User.findOne({ _id: req.user.userId }, function (err, user) {
            if (err) {
                res.json({
                    error: err.message
                })
            }
            else
            {
                if (!user) {
                    res.json({
                        error: "user not found"
                    })
                }
                else {

                    User.getFriends(user, {},null,{skip:parseInt(req.headers.index),limit: parseInt(req.headers.limit)}, function (err_, myfriends) {
                        if (err_) {
                            res.json({
                                error: err_.message
                            })
                        }
                        else
                        {
                            var username_list = [];
                            for (var i =0; i< myfriends.length; i++)
                            {
                                //console.log(myfriends[i].friend);
                                var json_object = {
                                    username : myfriends[i].friend.username,
                                    status : myfriends[i].status
                                }
                                username_list.push(json_object);
                            }
                            if (username_list.length > 0)
                            {

                                getUserProfileList(username_list, function(err, friend_list){
                                    if (err)
                                        res.json({
                                            error: err_.message
                                        })
                                    else {
                                        res.json({
                                            users: friend_list
                                        })
                                    }
                                });
                            }
                            else {
                                res.json({
                                    users: []
                                })
                            }

                        }

                    });
                }
            }
        });
    }
});
app.post('/api/v1/test',function(req, res){
    //var end = Date.now();
    var arr = [1, 2, 2, 3, 4, 5, 5, 5, 6, 7, 7, 8, 9, 10, 10];
    //var timeStamp = end.getTime();
    //console.log(timeStamp);
    var unique = arr.filter(function(elem, index, self) {
        return index == self.indexOf(elem);
    })
    console.log(unique);
    res.json({
        unique:unique
    });


});
app.post('/api/v1/requesttoken',function(req, res){

    res.json({
        message:'end'
    });
});
app.post('/api/v1/fcm', function (req, res){
    sendMessageToUser(req.body.token,req.body.content,req.body.title, function(err, result){
        if (err)
            res.json({
                error: err.message
            })
        else
            res.json({
                message: result
            })
    });
});
//---------------------Chat Socket--------------------------------------------
var hash_name_of_chat_rooms = new HashMap();//room (key:room_id, room_object)
var hash_users = new HashMap();//user (key:user_id_socket_id,user_object)
//---------------------Dinh nghia chat user-----------------------------------
function Chat_User_Object(_user_id,_token,_display_name,_avatar,_socket_id){
    this.id = _socket_id;
    this.user_id = _user_id;
    this.token = _token;
    this.display_name = _display_name;
    this.avatar = _avatar;
    this.socket_id = _socket_id;
    this.rooms_name = [];
}
Chat_User_Object.prototype.GetRooms = function(){
    return this.rooms_name;
}
Chat_User_Object.prototype.JoinRoom = function(room){
    var result = _.find(this.rooms_name, function (o) { return o == room; });
    if (!result)
    {

        this.rooms_name.push(room);
        //console.log(this.rooms_name);
    }
}
Chat_User_Object.prototype.RemoveRoom = function(room){
    this.rooms_name = _.without(this.rooms_name, room);
}
Chat_User_Object.prototype.Print = function(){
    console.log(this.id + ':'+this.rooms_name.length + ' room ---> '+ this.rooms_name);
}
//-----------------------------------------------------------
function Room_Object(_room_name,_chat_package){
    this.room_name = _room_name;
    this.chat_package_id = _chat_package;
    this.users_id = [];
}
Room_Object.prototype.GetUsers = function(){
    return this.users_id;
}
Room_Object.prototype.JoinUser = function(user){
    var result = _.find(this.users_id, function (o) { return o == user; });
    if (!result)
    {
        this.users_id.push(user);
    }
}
Room_Object.prototype.RemoveUser = function(user){
    this.users_id = _.without(this.users_id, user);
}
Room_Object.prototype.Print = function(){
    console.log(this.room_name + ':'+ this.users_id.length + ' user ---> '+ this.users_id);
}
Room_Object.prototype.IsUserIn = function(user){
    var result = _.find(this.users_id, function (o) { return o == user; });
    if (!result)
    {
        return false;
    }
    return true;
}
Room_Object.prototype.IsEmpty = function(){
    return (this.users_id.length == 0);
}
//-----------------------------------------------------------


function user_join_chat_room(_user_id,_token,_display_name,_avatar,_socket_id, room_name, socket, callback){
    console.log(room_name);
    var _room = hash_name_of_chat_rooms.get(room_name);
    if (!_room)//tao room
    {
        MissionPackage.findOne( {_id: room_name},{chatroom:1}, function(err, mission_package){
            if (err)
            {
                console.log('tim bi loi');
                callback(false)
            }
            else{
                if (!mission_package){
                    console.log('tim khong thay');
                    callback(false);
                }
                else {
                    console.log('tim  thay');
                    var new_room = new Room_Object(room_name);
                    new_room.JoinUser(_socket_id);//userid and socket_id
                    hash_name_of_chat_rooms.set(room_name,new_room);

                    //console.log('hash_name_of_chat_rooms -->add new key-->'+room_name);
                    //console.log(hash_name_of_chat_rooms);
                    var _user = hash_users.get(_socket_id);//userobject
                    if (!_user)//tao user
                    {
                        var new_user = new Chat_User_Object(_user_id,_token,_display_name,_avatar,_socket_id);
                        new_user.JoinRoom(room_name);
                        console.log('hash_users -->add new key-->'+_socket_id);
                        hash_users.set(_socket_id,new_user);
                    }
                    else
                    {
                        //console.log('!!!!!!!!!!!!!!!!!!!!1');
                        _user.JoinRoom(room_name);
                    }
                    socket.chatroom = mission_package.chatroom;
                    callback(true)
                }
            }
        });
    }
    else
    {
        var result = _room.IsUserIn(_socket_id)
        if (!result)
        {
            //console.log('debuddddddddgggggggggggg');
            _room.JoinUser(_socket_id);
            var _user = hash_users.get(_socket_id);//userobject
            if (!_user)//tao user
            {
                var new_user = new Chat_User_Object(_user_id,_token,_display_name,_avatar,_socket_id);
                new_user.JoinRoom(room_name);
                console.log('hash_users -->add new key-->'+_socket_id);
                hash_users.set(_socket_id,new_user);
            }
            else
            {
                //console.log('!!!!!!!!!!!!!!!!!!!!1');
                _user.JoinRoom(room_name);
            }
            callback(true)
        }
        else {
            console.log('yyyyyyyyyyyyyyyyyyy');
            callback(false)
        }

    }
}
function user_leave_chat_room(id, room_name){
    var _room = hash_name_of_chat_rooms.get(room_name);
    if (_room)
    {
        _room.RemoveUser(id);

        var _user = hash_users.get(id);

        if (_user)
        {
            _user.RemoveRoom(room_name);
            if (_room.IsEmpty())
            {
                //console.log(room_name + 'empty!!!');
                hash_name_of_chat_rooms.remove(room_name);
            }

        }

    }
}
CheckUserRoom = function(user_id, socket_id, room_id){
    var u = hash_users.get(user_id + socket_id);
    //console.log(user_id + socket_id);
    if (u)
    {

        var rerult = _.find(u.GetRooms(), function (o) { return o == room_id; });
        if (rerult)
        {
            //console.log('already in');
            return true;
        }
        else {

            return false;
        }
    }
    else
    {
        //console.log('not in');
        return false;
    }

}
//--------------------Start listen----------------------------------------------------

function intitChanelEvent(room_id, socket){
    console.log('start chat event in --->'+room_id);
    socket.on(room_id,function(chat_data) {

        if (utils.IsJsonString(chat_data))
        {

            var obj = JSON.parse(chat_data);
            console.log(socket.user_id);
            if (CheckUserRoom(socket.user_id, socket.id, obj.room))
            {
                utils.createChatObject();
                var newMsg = new ChatMessage({
                    lat: obj.lat,
                    long: obj.long,
                    username: obj.username,
                    content: obj.content,
                    room: room_id,
                    created: Date.now()
                });

                //Save it to database
                newMsg.save(function(err, msg){
                    //Send message to those connected in the room
                    //console.log(msg.room);
                    //obj.created = Date.now();
                    //io.in(socket.room_id).emit('chat mission', JSON.stringify(obj));
                    console.log('['+newMsg.room+']' + newMsg.username + ':' + newMsg.content);
                    socket.broadcast.to(room_id).emit('chat_message', JSON.stringify(newMsg));
                    //io.in(room_id).emit('chat_message', JSON.stringify(newMsg));
                });
            }
            else
            {
                console.log('khong co quyen chat vao room '+ obj.room);
            }
            //console.log(obj);

        }
        else {
            console.log(chat_data)
        }
    });
    socket.on(room_id+'_user_left',function(username) {
        console.log('left event');
        console.log('socket.user_id : '+ socket.user_id);
        console.log('socket.id : '+ socket.id);
        console.log('room_id : '+ room_id);
        if (CheckUserRoom(socket.user_id, socket.id, room_id))
        {

            user_leave_chat_room(socket.user_object_id, room_id);
            socket.broadcast.to(room_id).emit('user_left', {username:username,room:room_id});
            socket.leave(room_id);
            socket.emit('left room', {'room':room_id});
            //socket.removeAllListeners(room_id+'_user_left');
            console.log(socket.id + '--->has left from '+room_id);
        }
        else
            console.log('result not found');
    });
}
io.sockets.on('connection', function (socket) {
    console.log(colors.green(socket.id+ '----> on connection'));
    socket.rooms = new HashMap();
    //console.log(socket.id);
    socket.on('validate', function(user_object){

        utils.isValidate(user_object, function(result, user_id){
            if (result == true)
            {
                console.log(user_id + ' validated');
                socket.user_object_id = user_id + socket.id;
                socket.user_id = user_id;

                socket.emit('validate', {result:true});
            }
            else
            {
                socket.emit('validate', {result:false});
                io.sockets.connected[socket.id].disconnect();
            }
        });
    });

    socket.on('join room mission',function(data) {
        if (utils.IsJsonString(data) && socket.user_object_id)
        {
            var join_info = JSON.parse(data);
            var room = socket.rooms.get(join_info.room_id);
            if (!CheckUserRoom(socket.user_id, socket.id, join_info.room_id))
            {
                socket.username = join_info.user_id;
                socket.join(join_info.room_id);
                console.log(colors.green(socket.id+ '----> on join room' + join_info.room_id));
                //insert to user room hashmap
                user_join_chat_room(join_info.user_id, join_info.token,'','',socket.user_object_id, join_info.room_id, socket, function(result){
                    console.log('-->result-->'+result);
                    if (result == true) {
                        socket.chat_package_id = result;
                        console.log('socket.chat_package_id-->'+socket.chat_package_id);
                        if (!room)
                        {
                            socket.rooms.set(join_info.room_id,join_info.room_id);
                            intitChanelEvent(join_info.room_id, socket);
                        }
                        else
                            console.log(join_info.room_id + ' old event');

                        var room_object = hash_name_of_chat_rooms.get(join_info.room_id)
                        room_object.Print();
                        socket.broadcast.to(join_info.room_id).emit('user_join', {username:join_info.user_id,room:join_info.room_id});

                        socket.emit('join room', {'room':join_info.room_id});
                    }
                    //set up chat event

                });

            }
            else {
                console.log('debug this way');
            }
        }
        else {
            console.log('join fail');
            var join_info = JSON.parse(data);
            console.log(socket.user_object_id);
            socket.emit('join room', {'room':join_info.room_id,result:false});
        }


    });


    socket.on('disconnect', function(){
        console.log(socket.id+'--->disconnected');
        if (socket.user_object_id)
        {
            console.log(socket.user_object_id+'--->disconnected');
            //console.log(hash_users);
            //console.log('---------');
            var user_object = hash_users.get(socket.user_object_id);
            //
            if (user_object){


                var room = user_object.GetRooms();
                console.log('------user disconnected room is---');
                //console.log(room);
                room.forEach(function(value, key) {
                    console.log(socket.username + " left : " + value);
                    user_leave_chat_room(socket.user_object_id, value);
                    socket.broadcast.to(value).emit('user_left', {username:socket.username,room:value});
                });
                hash_users.remove(socket.user_object_id);
            }
//            else {
//                console.log('not found');
//            }
        }
    });
});

server.listen(config.get('port'));
console.log('Listening port: ' + config.get('port'));

User.updateMany({}, {$set: {state: false}}, function (error_, numberAffected) {
    if (error_)
        console.log('start error');
    else {
        console.log('all user is now offline');
    }
});
