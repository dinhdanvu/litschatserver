var asyncLoop = require('node-async-loop');
var HashMap = require('hashmap');
var hash_name_of_chat_rooms = new HashMap();
var hash_users = new HashMap();
//---------------------Dinh nghia chat user-----------------------------------
function Chat_User_Object(_user_id,_token,_display_name,_avatar,_socket_id){
    this.id = _user_id+_socket_id;
    this.user_id = _user_id;
    this.token = _token;
    this.display_name = _display_name;
    this.avatar = _avatar;
    this.socket_id = _socket_id;
    this.rooms_name = new HashMap();
}
Chat_User_Object.prototype.GetRooms = function(){
    return this.rooms_name;
}
Chat_User_Object.prototype.JoinRoom = function(room){
    var r = this.rooms_name.get(room);
    if (!r)
        this.rooms_name.set(room,room);
}
Chat_User_Object.prototype.RemoveRoom = function(room){
    var r = this.rooms_name.get(room);
    if (r)
        this.rooms_name.remove(r);
}
//-----------------------------------------------------------
function Room_Object(_room_name){
    this.room_name = _room_name;
    this.users_id = new HashMap();
}
Room_Object.prototype.GetUsers = function(){
    return this.users_id;
}
Room_Object.prototype.JoinUser = function(user){
    var u = this.users_id.get(user);
    if (!u)
        this.users_id.set(user, user);
}
Room_Object.prototype.RemoveUser = function(user){
    var u = this.users_id.get(user);
    if (u)
        this.users_id.remove(u);
}
//-----------------------------------------------------------

function user_join_chat_room(_user_id,_token,_display_name,_avatar,_socket_id, room_name){
    var _room = hash_name_of_chat_rooms.get(room_name);
    if (!_room)
    {
        var new_room = new Room_Object(room_name);
        new_room.JoinUser(_user_id+_socket_id);
        hash_name_of_chat_rooms.set(room_name,new_room);
        var _user = hash_users.get(_user_id+_socket_id);
        if (!_user)
        {
            var new_user = new Chat_User_Object(_user_id,_token,_display_name,_avatar,_socket_id);
            new_user.JoinRoom(room_name);
            hash_users.set(_user_id+_socket_id,new_user);
        }
        else
        {
            _user.JoinRoom(room_name);
        }
    }
    else
    {
        _room.JoinUser(_user_id+_socket_id);
    }
}
function user_leave_chat_room(_user_id,_socket_id, room_name){
    var _room = hash_name_of_chat_rooms.get(room_name);
    if (_room)
    {
        _room.RemoveUser(_user_id+_socket_id);

        var _user = hash_users.get(_user_id+_socket_id);
        if (_user)
        {
            _user.RemoveRoom(room_name);
        }
    }
}

user_join_chat_room("_user_id1","_token1","_display_name1","_avatar1","_socket_id1", "room_name_A");
user_join_chat_room("_user_id1","_token1","_display_name1","_avatar1","_socket_id1", "room_name_B");

console.log(hash_name_of_chat_rooms);
console.log(hash_users);
console.log('----------------------------------');

user_leave_chat_room("_user_id1","_socket_id1", "room_name_A")
console.log(hash_name_of_chat_rooms);
console.log(hash_users);
console.log('----------------------------------');

var room_name_B = hash_name_of_chat_rooms.get("room_name_A");
console.log(room_name_B);
var _user_id1 = room_name_B.users_id.get("_user_id1"+"_socket_id1");
console.log(_user_id1);
user_join_chat_room("_user_id1","_token1","_display_name1","_avatar1","_socket_id1", "room_name_A");
_user_id1 = room_name_B.users_id.get("_user_id1"+"_socket_id1");
console.log(_user_id1);